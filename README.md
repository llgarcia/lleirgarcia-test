# Signaturit tests #

## Pre requirements ##

- Java JDK 8
- Maven 3.3

## Steps  

To execute all of scenarios:

1 way:

- Execute "mvn test" in command line.

2 way:

- Go to "src/test/java/com/signaturit/CucumberRunnerTest.java"

- Do right click and "Run".

Get reports: 

- Execute "mvn cluecumber-report:reporting" to generate report (Cluecumber plugin)

- Go to  "/target/generated-report" and open "index.html" in browser.

To execute one scenario:

- Go to feature file "src/test/java/com/signaturit/features/*.feature"

- Right click on an screnario and "Run".

### Info? ###
 
You have info searching for "TODO" in the code. There are TODOs in almost clases to understand what a classe does.

### Doubts? ###

lleirgarcia@gmail.com