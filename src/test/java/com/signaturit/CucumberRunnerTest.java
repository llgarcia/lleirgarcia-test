package com.signaturit;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/com/signaturit/features/"},
        glue={
                "com.signaturit.core.hooks",
                "com.signaturit.steps"
        },
        strict=true,
        monochrome=true,
        plugin = {"json:target/cucumber-report/cucumber.json"}
)
public class CucumberRunnerTest {
}
