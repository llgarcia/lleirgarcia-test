Feature: Error validation management

  Background: Open Signaturit register form page
    Given I open "https://app.signaturit.com/en/register/business" on the browser
    Then I am in "/register/business" page

  Scenario: Error displayed registering user with empty fields
    When I fill the form field '<field>' with 'empty value'
    And I select 'I accept the privacy policy and Terms and conditions'
    And I click on 'submit'
    Then I see the error 'first name' in 'Invalid name' form field
    And I see the error "last name" in "Invalid surname" form field
    And I see the error "Country" in "The country can't be empty" form field
    And I see the error "last name" in "Invalid surname" form field
    And I see the error "last name" in "Invalid surname" form field
    And I see the error "Employees number" in "The employees number can't be empty" form field
    And I see the error "Who do we need to sign documents?" in "This field can't be empty" form field
    And I see the error "How do you generate the documents to sign?" in "This field can't be empty" form field
    And I see the error "How many documents did you send to be signed las mont?" in "This field can't be empty" form field
    And I see the error "Job title" in "The job title can't be empty" form field
    And I see the error "Phone number" in "The phone can't be empty" form field
    And I see the error "Business email" in "The email is invalid" form field

  Scenario: Error displayed registering user with bad phone number format
    Given I fill the register form
    When I fill the form field 'Phone number' with "bla bla bla"
    And I select 'I accept the privacy policy and Terms and conditions'
    And I click on 'submit'
    Then I see the error 'This value is not valid.' in 'phone number' form field

  Scenario Outline: Error displayed registering user with bad business email format
    Given I fill the register form
    When I fill the form field 'Business email' with '<email>'
    And I select 'I accept the privacy policy and Terms and conditions'
    And I click on 'submit'
    Then I see the error 'The email is invalid' in 'Business email' form field
    Examples:
      | email     |
      | bla bla   |
      | lleir@    |
      | lleir@.es |
      | @gmail.es |

  Scenario: Error displayed registering with bad format password
    Given I fill the register form
    When I fill the form field 'Password' with "baad format"
    And I select 'I accept the privacy policy and Terms and conditions'
    And I click on 'submit'
    Then I see the error 'Your password requires: a number' in 'Password' form field
