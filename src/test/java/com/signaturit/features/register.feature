Feature: User register

  #  Reminder: Submit is not available! I've commented in some scenarios or methods.

  Background: Open Signaturit register form page
  Given I open "https://app.signaturit.com/en/register/business" on the browser
  Then I am in "/register/business" page

  Scenario: Register user with valid data
    When I fill the form field 'First name' with 'Lleir'
    And I fill the form field 'Last name' with 'Garcia'
    And I fill the form field 'Company' with 'Signaturit'
    And I fill the form field 'Country' with 'Spain'
    And I fill the form field 'Employees Number' with '11-50'
    And I fill the form field 'Who do we need to sign documents?' with 'Personnel of my company (employees or collaborators).'
    And I fill the form field 'How do you generate the documents to sign?' with 'Manually for each signature'
    And I fill the form field 'How many documents did you send to be signed las mont?' with 'between 101 and 500'
    And I fill the form field 'Job title' with 'QA'
    And I fill the form field 'Phone number' with '657645345'
    And I fill the form field 'Business email' with 'pepe@gmail.com'
    And I fill the form field 'Password' with 'p3p3'
    And I select 'I accept the privacy policy and Terms and conditions'
#    And I click on 'submit'
    Then I see the user registered correctly

#    TODO imperative way
#    is just an example of the "imperative" way, with the same scenario of above.
#    for me the way to define an scenario depends to the aim of that.
#    Imperative way is always readable and simpler than declarative.
  Scenario: Register user with valid data "imperative way"
    When I fill the register form
    And I submit the form accepting policy and privacy terms
    Then I see the user registered correctly

