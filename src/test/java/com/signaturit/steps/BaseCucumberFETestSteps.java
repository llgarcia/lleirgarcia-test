package com.signaturit.steps;

import com.signaturit.application.SignaturitSite;

public abstract class BaseCucumberFETestSteps<Type> {

    public static SignaturitSite site;

    public static SignaturitSite getIntantiateObject() {
        return new SignaturitSite();
    }

    public static Object getIntantiateObject(Object page) {
        return new SignaturitSite(true).init(page);
    }

}
