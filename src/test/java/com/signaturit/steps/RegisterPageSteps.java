package com.signaturit.steps;

import com.signaturit.application.Log;
import com.signaturit.application.SignaturitSite;
import com.signaturit.application.entities.RegisterDataEntity;
import com.signaturit.application.entities.RegistryEntity;
import com.signaturit.application.pages.RegisterPage;
import com.signaturit.core.selenium.BrowserInstance;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * TODO RegisterPage
 * There are the steps.
 * RegisterPage is always calling that we have some action to do in some component.
 */
public class RegisterPageSteps extends BaseCucumberFETestSteps<SignaturitSite> {

    public RegisterPage registerPage() {
        return (RegisterPage) getIntantiateObject(new SignaturitSite().registerPage);
    }

    @Given("I open {string} on the browser")
    public void goTOSignUpPage(String url) {
        BrowserInstance.getDriver().get(url);
        Log.info("Url ["+url+"] opened");
    }

    @Then("I am in {string} page")
    public void validateUExpectedUrl(String url) {
        BrowserInstance.assertCurrentUrlEndsWith(url);
        Log.info("We are in ["+url+"]");
    }

    @When("I fill the register form")
    public void fillTheRegisterForm () {
        RegistryEntity entity = RegisterDataEntity.loadRandom();
        fillTheFormFieldWith(RegisterPage.FIELDFORM_NAMEFIRSTNAME, entity.getFirstName());
        fillTheFormFieldWith(RegisterPage.FIELDFORM_NAMELASTNAME, entity.getLastName());
        fillTheFormFieldWith(RegisterPage.FIELDFORM_NAMECOMPANY, entity.getCompany());
        fillTheFormFieldWith(RegisterPage.FIELDFORMNAMECOUNTRY, entity.getCountry());
        fillTheFormFieldWith(RegisterPage.FIELDFORMNAMEEMPLOYEEN, entity.getEmployees());
        fillTheFormFieldWith(RegisterPage.FIELDFORMNAMEWHO, entity.getWho());
        fillTheFormFieldWith(RegisterPage.FIELDFORMNAMEHOWGENERATE, entity.getHowGenerate());
        fillTheFormFieldWith(RegisterPage.FIELDFORMNAMEHOWMANY, entity.getHowManyDocs());
        fillTheFormFieldWith(RegisterPage.FIELDFORMNAMEJOBTITLE, entity.getJobTitle());
        fillTheFormFieldWith(RegisterPage.FIELDFORMNAMEPHONENUMBER, entity.getPhoneNumber());
        fillTheFormFieldWith(RegisterPage.FIELDFORMNAMEBUSINESSEMAIL, entity.getBusinessEmail());
        fillTheFormFieldWith(RegisterPage.FIELDFORMNAMEPASSWORD, entity.getPassword());
        Log.info("All fields in the form were filled with valid values.");
    }

    @When("I fill the form field {string} with {string}")
    public void fillTheFormFieldWith(String formField, String value) {
        registerPage().fillFieldByValue(formField, value);
        Log.info("Form field ["+formField+"] was filled with value ["+value+"]");
    }

    @When("I submit the form accepting policy and privacy terms")
    public void acceptAndSubmit() {
        selectAcceptTerms();
//        clickOnSubmit(); // commented here to not proceed to the register
    }

    @When("I select 'I accept the privacy policy and Terms and conditions'")
    public void selectAcceptTerms() {
        BrowserInstance.clickOn(registerPage().privacyPolicy);
        Log.info("Privacy policy and terms and conditions were clicked.");
    }

    @When("I click on 'submit'")
    public void clickOnSubmit() {
        BrowserInstance.clickOn(registerPage().submit);
        Log.info("Submit form button was clicked.");
    }

    @Then("I see the error {string} in {string} form field")
    public void validateErrorInFormField(String expectedError, String formField) {
        registerPage().validateExpectedError(expectedError, formField);
        Log.info("The field ["+formField+"] has the expected error ["+expectedError+"]");
    }

    @Then("I do not see any validation error message")
    public void validateNoErrorsInFormFields() {
        registerPage().validateThereAreNoErrores();
        Log.info("There are any errors in the form.");
    }

    @Then("I see the user registered correctly")
    public void validateUserRegisteredPage() {
        // not needed
    }

}
