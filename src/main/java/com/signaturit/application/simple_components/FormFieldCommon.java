package com.signaturit.application.simple_components;

import com.signaturit.core.component_factory.BaseWebComponent;
import com.signaturit.core.component_factory.ComponentInfo;
import org.openqa.selenium.support.FindBy;

public class FormFieldCommon extends BaseWebComponent {

    @FindBy(css = ".error small")
    @ComponentInfo(nameField = "span")
    public Div errorMessage;

    @FindBy(css = ".error i")
    @ComponentInfo(nameField = "span")
    public Div errorIcon;
}
