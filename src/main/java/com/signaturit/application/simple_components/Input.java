package com.signaturit.application.simple_components;

import com.signaturit.core.component_factory.BaseWebComponent;

public class Input extends BaseWebComponent {
    boolean isMandatoy;

    public boolean isMandatoy() {
        return isMandatoy;
    }

    public void setMandatoy(boolean mandatoy) {
        isMandatoy = mandatoy;
    }
}
