package com.signaturit.application.simple_components;

import com.signaturit.core.component_factory.BaseWebComponent;
import com.signaturit.core.component_factory.ComponentInfo;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class FormInput extends FormFieldCommon {
    @FindBy(css = "input")
    @ComponentInfo(nameField = "input form field")
    public Input input;

}