package com.signaturit.application.simple_components;

import com.signaturit.core.component_factory.BaseWebComponent;
import com.signaturit.core.component_factory.ComponentInfo;
import org.openqa.selenium.support.FindBy;

public class FormSelect extends FormFieldCommon {
    @FindBy(css = "select")
    @ComponentInfo(nameField = "select form field")
    public Select select;
}
