package com.signaturit.application.entities;

/** TODO Data entities
 * A very easy example of loading values "random" (I know, it is not random...) to be used in a test without
  * the needs to be inserted by scenario itself. We could also use a JSON for example or another input data, like get
 * it by the BBDD.
 */
public class RegisterDataEntity {

    public static RegistryEntity loadRandom() {
        RegistryEntity entity = new RegistryEntity();
        entity.setFirstName("Pepe");
        entity.setLastName("Rodriguez");
        entity.setCompany("Signaturit");
        entity.setCountry("Spain");
        entity.setHowGenerate("Manually for each signature");
        entity.setHowManyDocs("between 101 and 500");
        entity.setJobTitle("CEO");
        entity.setPhoneNumber("627182839");
        entity.setPassword("passw0rd");
        entity.setBusinessEmail("email@gmail.es");
        entity.setEmployees("501-1000");
        entity.setWho("Both my company's and external staff.");
        return entity;
    }
}
