package com.signaturit.application.entities;

/**
 * TODO Entities
 *
 * Entities for me is a way to manage data inside an scenario. Can be built of data from the scenario or
 * loaded while we are executing an scenario by a JSON, bbdd calls, etc.
 */
public class RegistryEntity {

    private String firstName;
    private String lastName;
    private String company;
    private String country;
    private String employees;
    private String who;
    private String howGenerate;
    private String howManyDocs;
    private String jobTitle;
    private String phoneNumber;
    private String businessEmail;
    private String password;

    public RegistryEntity(){};

    public RegistryEntity(String firstName, String lastName, String company, String country, String employees, String who, String howGenerate, String howManyDocs, String jobTitle, String phoneNumber, String businessEmail, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.country = country;
        this.employees = employees;
        this.who = who;
        this.howGenerate = howGenerate;
        this.howManyDocs = howManyDocs;
        this.jobTitle = jobTitle;
        this.phoneNumber = phoneNumber;
        this.businessEmail = businessEmail;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmployees() {
        return employees;
    }

    public void setEmployees(String employees) {
        this.employees = employees;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getHowGenerate() {
        return howGenerate;
    }

    public void setHowGenerate(String howGenerate) {
        this.howGenerate = howGenerate;
    }

    public String getHowManyDocs() {
        return howManyDocs;
    }

    public void setHowManyDocs(String howManyDocs) {
        this.howManyDocs = howManyDocs;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBusinessEmail() {
        return businessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = businessEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
