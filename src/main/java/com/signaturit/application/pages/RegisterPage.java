package com.signaturit.application.pages;

import com.google.gson.JsonElement;
import com.signaturit.application.simple_components.Button;
import com.signaturit.application.simple_components.FormInput;
import com.signaturit.application.simple_components.FormSelect;
import com.signaturit.application.simple_components.Input;
import com.signaturit.core.component_factory.ComponentInfo;
import com.signaturit.core.component_factory.WebPage;
import com.signaturit.core.selenium.BrowserInstance;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.support.FindBy;

import java.lang.reflect.Field;

/**
 * TODO Page with components
 *
 * This class contains all components of register page. They are wrapped objects of web components.
 * @FindBy defines how we get the element and the locator to reach it.
 * @ComponentInfo defined the real name that the fields have in the web page.
 */
public class RegisterPage extends WebPage {
    
    public static final String FIELDFORM_NAMEFIRSTNAME = "First name";
    public static final String FIELDFORM_NAMELASTNAME = "Last name";
    public static final String FIELDFORM_NAMECOMPANY = "Company";
    public static final String FIELDFORMNAMECOUNTRY = "Country";
    public static final String FIELDFORMNAMEEMPLOYEEN = "Employees Number";
    public static final String FIELDFORMNAMEWHO = "Who do we need to sign documents?";
    public static final String FIELDFORMNAMEHOWGENERATE = "How do you generate the documents to sign?";
    public static final String FIELDFORMNAMEHOWMANY = "How many documents did you send to be signed last month ?";
    public static final String FIELDFORMNAMEJOBTITLE = "Job title";
    public static final String FIELDFORMNAMEBUSINESSEMAIL = "Business Email";
    public static final String FIELDFORMNAMEPHONENUMBER = "Phone Number";
    public static final String FIELDFORMNAMEPASSWORD= "Password";

//    TODO here could be a @FindByInApp/@FindByInWeb
//    or even inside a custom FindBy set a locator for app or web.
    @FindBy(xpath = "//input[@id='register_first_name']/parent::div")
    @ComponentInfo(nameField = FIELDFORM_NAMEFIRSTNAME)
    public FormInput firstName;

    @FindBy(xpath ="//input[@id='register_last_name']/parent::div")
    @ComponentInfo(nameField = FIELDFORM_NAMELASTNAME)
    public FormInput lastName;

    @FindBy(xpath ="//input[@id='register_company']/parent::div")
    @ComponentInfo(nameField = FIELDFORM_NAMECOMPANY)
    public FormInput companyName;

    @FindBy(xpath ="//select[@id='countries']/parent::div")
    @ComponentInfo(nameField = FIELDFORMNAMECOUNTRY)
    public FormSelect country;
    
    @FindBy(xpath ="//select[@id='employees_number']/parent::div")
    @ComponentInfo(nameField = FIELDFORMNAMEEMPLOYEEN)
    public FormSelect employeeNumber;

    @FindBy(xpath ="//select[@id='signers_type']/parent::div")
    @ComponentInfo(nameField = FIELDFORMNAMEWHO)
    public FormSelect whoNeedSign;

    @FindBy(xpath ="//select[@id='sign_mode']/parent::div")
    @ComponentInfo(nameField = FIELDFORMNAMEHOWGENERATE)
    public FormSelect howDoGenerateSign;

    @FindBy(xpath ="//select[@id='documents_quantity']/parent::div")
    @ComponentInfo(nameField = FIELDFORMNAMEHOWMANY)
    public FormSelect howManyDocs;

    @FindBy(xpath ="//input[@id='register_job_title']/parent::div")
    @ComponentInfo(nameField = FIELDFORMNAMEJOBTITLE)
    public FormInput jobTitle;

    @FindBy(xpath ="//input[@id='register_phone']/parent::div")
    @ComponentInfo(nameField = FIELDFORMNAMEPHONENUMBER)
    public FormInput phoneNumber;

    @FindBy(xpath ="//input[@id='register_email']/parent::div")
    @ComponentInfo(nameField = FIELDFORMNAMEBUSINESSEMAIL)
    public FormInput bussinesEmail;

    @FindBy(xpath ="//input[@id='register_password']/parent::div")
    @ComponentInfo(nameField = FIELDFORMNAMEPASSWORD)
    public FormInput password;

    @FindBy(css = "input[data-sg-gdpr-attribute='privacy']")
    @ComponentInfo(nameField = "accept")
    public Input privacyPolicy;

    @FindBy(css = "button[id='register_submit']")
    @ComponentInfo(nameField = "accept")
    public Button submit;

    /**
     * Method to write/select text by form field. Just is needed the "field name" (the same that in the app) and the value
     * to fill. This "field name" will be mached with @ComponentInfo(nameField = "") in order to know which field do we want
     * to fill.
     *
     * With that we let the "step" as clean as possible.
     *
     * For me, the behavior of the component should be in the same component class.
     *
     * I also use BrowserInstance as a "bot" inside the browser. It have lots of wrapped methods.
     */
    public void fillFieldByValue(String field, String value) {
        Field[] fields = this.getClass().getDeclaredFields();

        if(StringUtils.isNotEmpty(value)) {
            try {
                for (Field f : fields) {
                    if (!(f.get(this) instanceof String)) {
                        String a = f.getAnnotation(ComponentInfo.class).nameField();
                        if (StringUtils.containsIgnoreCase(a, field)) {
                            if (f.get(this) instanceof FormInput) {
                                if(StringUtils.equalsIgnoreCase(value, "empty value")) {
                                    BrowserInstance.clear(((FormInput) f.get(this)).input);
                                } else {
                                    BrowserInstance.clearAndType(value, ((FormInput) f.get(this)).input);
                                }
                            } else if (f.get(this) instanceof FormSelect) {
                                if(StringUtils.equalsIgnoreCase(value, "empty value")){
                                    BrowserInstance.deselectAllOptions(((FormSelect) f.get(this)).select);
                                } else {
                                    BrowserInstance.selectOption(value, ((FormSelect) f.get(this)).select);
                                }
                            }

                            break;
                        }
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method to validate a form field error by its form field name.
     */
    public void validateExpectedError(String expected, String formField) {
        Field[] fields = this.getClass().getDeclaredFields();
        try {
            for(Field f : fields) {
                    if(!(f.get(this) instanceof String)) {
                        String a = f.getAnnotation(ComponentInfo.class).nameField();
                         if (StringUtils.containsIgnoreCase(a, formField)) {
                            if (f.get(this) instanceof FormInput) {
                                Assert.assertEquals("Form field [" + formField + "] does not have expected validation error"
                                                + "[" + expected + "]",
                                        expected, ((FormInput) f.get(this)).errorMessage.getText()
                                );
                                Assert.assertTrue(((FormInput) f.get(this)).errorIcon.isDisplayed());
                            } else if (f.get(this) instanceof FormSelect) {
                                Assert.assertEquals("Form field [" + formField + "] does not have expected validation error"
                                                + "[" + expected + "]",
                                        expected, ((FormSelect) f.get(this)).errorMessage.getText()
                                );
                                Assert.assertTrue(((FormInput) f.get(this)).errorIcon.isDisplayed());
                            }

                            break;
                        }
                    }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void validateThereAreNoErrores() {
        Field[] fields = this.getClass().getDeclaredFields();
        int count = 0;

        try {
            for(Field f : fields) {
                if(!(f.get(this) instanceof String)) {
                    if (f.get(this) instanceof FormInput) {
                        count = (!(((FormInput) f.get(this)).errorMessage.isDisplayed()) ? 0 : 1);
                    } else if (f.get(this) instanceof FormSelect) {
                        count = (!(((FormSelect) f.get(this)).errorMessage.isDisplayed()) ? 0 : 1);
                    }
                    if(count == 1)
                        break;
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("There are some errors in a form field.", count, 0);
    }
}
