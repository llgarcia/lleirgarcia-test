package com.signaturit.application;

import com.signaturit.core.component_factory.PageFactoryCustom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log {

    private static final Logger log = LoggerFactory.getLogger(PageFactoryCustom.class);

    public static void info(String msg) {
        log.info(msg);
    }

}
