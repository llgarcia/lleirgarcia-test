package com.signaturit.core.selenium;

import com.github.webdriverextensions.WebDriverProperties;
import com.signaturit.core.selenium.BrowserInstance;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * TODO Build the Driver.
 * Class to manage the initiation of the browser. Configuring their properties.
 */

public class BrowseFactory {
    private static final Logger log = LoggerFactory.getLogger(BrowseFactory.class);

    private String browser;
    private boolean isGrid;
    private String url;
    private String osName;
    private String driverPath;

    public BrowseFactory(Properties prop) {
        browser = prop.getProperty("selenium.browser");
        url = prop.getProperty("browser.url");
        driverPath = prop.getProperty("driver.path");
        osName = System.getProperty("os.name");
        isGrid = Boolean.parseBoolean(prop.getProperty("selenium.grid"));
    }

    /**
     * Initialize a instance of the target browser.
     * This selection will be specify in current context
     */
    public void openBrowser() {

        switch (browser){
            case "chrome":
            case "ch":
                loadChromeDriver();
                break;

            case "firefox":
            case "mozilla":
            case "mozilla firefox":

                setFirefoxSettings();
                break;

            case "ie":
            case "explorer":
            case "internet explorer":

                setIESettings();
                break;

        }
    }

    private static void setFirefoxSettings() {}
    private static void setIESettings() {}

    private void loadChromeDriver() {
        ChromeDriver driver = null;
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--lang=en-GB");

        if (isGrid) {
            log.info("Using selenium grid.");
//            configure by the needs.
        } else {
            log.info("Not using selenium grid.");

            if(StringUtils.containsIgnoreCase(osName, "windows")) {
                driverPath = driverPath + ".exe";
                System.setProperty(WebDriverProperties.CHROME_DRIVER_PROPERTY_NAME, driverPath);
                log.info("Driver path for windows done.");
            } else if(StringUtils.containsIgnoreCase(osName, "mac")){
                System.setProperty(WebDriverProperties.CHROME_DRIVER_PROPERTY_NAME, driverPath);
                log.info("Driver path linux done.");
            }

            driver = new ChromeDriver(chromeOptions);
        }
        BrowserInstance.setDriver(driver);
    }

}
