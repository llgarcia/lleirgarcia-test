package com.signaturit.core.selenium;

import com.github.webdriverextensions.WebComponent;
import com.github.webdriverextensions.exceptions.WebAssertionError;
import com.github.webdriverextensions.internal.BotUtils;
import com.github.webdriverextensions.internal.Openable;
import com.github.webdriverextensions.internal.WebComponentList;
import com.github.webdriverextensions.internal.WebDriverExtensionException;
import com.github.webdriverextensions.internal.utils.NumberUtils;
import com.github.webdriverextensions.internal.utils.WebDriverUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * TODO Browser actions management
 * A summary of actions and/or validations that a the "bot" can do.
 */
public class BrowserInstance {

    private static final Logger log = LoggerFactory.getLogger(com.github.webdriverextensions.Bot.class);
    
    private static WebDriver driver;

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(WebDriver d) {
        driver = d;
    }

    public static void click(WebElement webElement) {
        webElement.click();
    }

    public static void doubleClick(WebElement webElement) {
        Actions action = new Actions(getDriver());
        action.doubleClick(webElement).perform();
    }

    public static void type(String text, WebElement webElement) {
        if (text != null) {
            webElement.sendKeys(new CharSequence[]{text});
        }
    }

    public static void hover(WebElement webElement) {
        Actions action = new Actions(getDriver());
        action.moveToElement(webElement).build().perform();
    }

    public static void clickOutisdeOfElement(int xOffset, int yOffset) {
        Actions actions = new Actions(getDriver());
        actions.moveByOffset(xOffset, yOffset).click().build().perform();
    }
    public static void type(double number, WebElement webElement) {
        type(NumberUtils.toString(number), webElement);
    }

    public static void clear(WebElement webElement) {
        webElement.sendKeys(" ");
        webElement.clear();
    }

    public static void clearAndType(String text, WebElement webElement) {
        clear(webElement);
        type(text, webElement);
    }

    public static void clearAndType(double number, WebElement webElement) {
        clear(webElement);
        type(number, webElement);
    }

    public static void clickOn(WebElement webElement) {
        scrollTo(webElement);
        click(webElement);
    }

    public static void hoverOn(WebElement webElement) {
        scrollTo(webElement);
        hover(webElement);
    }


    public static void hoverAndClick(WebElement webElement) {
        Actions actions = new Actions(getDriver());
        actions.moveToElement(webElement).click(webElement).build().perform();
    }


    public static void clickOnAndWait(WebElement webElement, Integer seconds) {
        clickOn(webElement);
        waitFor(seconds);
    }

    public static void switchToIFrame(String frame) {
        getDriver().switchTo().frame(frame);
    }

    public static void switchToIFrame(WebElement frame) {
        getDriver().switchTo().frame(frame);
    }

    public static void typeOn(WebElement webElement, String charSequence) {
        scrollTo(webElement);
        clearAndType(charSequence, webElement);
    }

    public static void typeOnWithoutClear(WebElement webElement, String charSequence) {
        scrollTo(webElement);
        type(charSequence, webElement);
    }

    public static void pressTab(WebElement webElement) {
        pressKeys(webElement, Keys.TAB);
    }

    public void typeOnLetterByLetter(WebElement webElement, String value, long waitBetweenLetters, ChronoUnit unitTime) {
        clear(webElement);
        Arrays.asList(value.toCharArray()).forEach(letter -> {
            typeOn(webElement, String.valueOf(letter));
            pause(waitBetweenLetters, unitTime);
        });
    }

    private void pause(long waitBetweenLetters, ChronoUnit unitTime) {
        try {
            Thread.sleep(Duration.of(waitBetweenLetters, unitTime).toMillis());
        } catch (InterruptedException ignore) {
        }
    }

    public static void typeOnCharToChar(String keys, WebElement webElement) {
        clear(webElement);
        for (int i = 0; i < keys.length(); i++) {
            webElement.sendKeys(Character.toString(keys.charAt(i)));
            final String word = keys.substring(0, i);
            waitUntil((ExpectedCondition<Boolean>) d -> attributeContains("value", word, webElement));
        }
    }


    public static void removeAttr(String attr, WebElement element) throws Exception {
        executeJavascript("arguments[0].removeAttribute('" + attr + "')", element);
    }


    public static void setAttr(String attr, String value, WebElement element) throws Exception {
        executeJavascript("arguments[0].setAttribute('" + attr + "','" + value + "')", element);
    }


    public static Actions dragElementHorizontally(WebElement element, Actions action) throws Exception {
        return action.clickAndHold(element)
                .moveByOffset(element.getSize().width, 0);
    }

    public static Actions dragElementHorizontally(WebElement element, Integer xOffset, Actions action) throws Exception {
        return action.clickAndHold(element)
                .moveByOffset(xOffset, 0);
    }

    public static Actions dragElementVerticaally(WebElement element, Actions action) throws Exception {
        return action.clickAndHold(element)
                .moveByOffset(0, element.getSize().height);

    }

    public static Actions dragElementVerticaally(WebElement element, Integer yOffset, Actions action) throws Exception {
        return action.clickAndHold(element)
                .moveByOffset(0, yOffset);
    }

    public static void selectMultipleOptionByValue(WebElement select, List<String> values) {
        List<WebElement> options = new Select(select).getOptions();
        Actions actions = new Actions(getDriver());
        if (platformIsMac()) {
            actions.keyDown(Keys.COMMAND);
        } else {
            actions.keyDown(Keys.CONTROL);
        }
        for (String value : values) {
            options.stream()
                    .filter(option -> valueContains(value, option))
                    .findFirst().ifPresent(actions::click);
        }
        if (platformIsMac())
            actions.keyUp(Keys.COMMAND);
        else
            actions.keyUp(Keys.CONTROL);

        actions.build().perform();
    }

    public static void selectMultipleOptionByText(WebElement select, List<String> values) {
        List<WebElement> options = new Select(select).getOptions();
        Actions actions = new Actions(getDriver());
        if (platformIsMac()) {
            actions.keyDown(Keys.COMMAND);
        } else {
            actions.keyDown(Keys.CONTROL);
        }
        for (String value : values) {
            options.stream()
                    .filter(option -> textEqualsIgnoreCase(value, option))
                    .findFirst().ifPresent(actions::click);
        }
        if (platformIsMac())
            actions.keyUp(Keys.COMMAND);
        else
            actions.keyUp(Keys.CONTROL);

        actions.build().perform();
    }


    public static void waitUntil(ExpectedCondition<Boolean> booleanExpectedCondition) {
        waitUntil(booleanExpectedCondition, 30);
    }

    public static void waitUntil(ExpectedCondition<Boolean> condition, long secondsToWait) {
        new WebDriverWait(getDriver(), secondsToWait).until(condition);
    }

    public static void waitForElementDisplay(WebElement e) {
        waitForElementToDisplay(e, 30);
    }

    public static void waitForElementNotDisplay(WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(ExpectedConditions.invisibilityOf(webElement));
    }

    public static void waitForElementNotDisplay(WebElement webElement, long secondsToWait) {
        WebDriverWait wait = new WebDriverWait(getDriver(), secondsToWait);
        List<WebElement> elements = new ArrayList<>();
        elements.add(webElement);
        wait.until(ExpectedConditions.invisibilityOfAllElements(elements));
    }

    public static void waitForElementToDisplay(By by) {
        waitForElementToDisplay(by, 30);
    }


    public static void waitForElementToClickable(WebElement webElement, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static void waitForElementToClickable(WebElement webElement) {
        waitForElementToClickable(webElement, 10);
    }

    public static WebElement waitForElementToDisplay(By by, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
        return wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }


    public static void waitForElementToClickable(By by, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }


    public static void waitUntilTextOnElementIsFilled(WebElement webElement) {
        waitUntilTextOnElementIsFilled(webElement, 30);
    }

    public static void waitUntilTextOnElementIsFilled(WebElement webElement, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
        wait.until((ExpectedCondition<Boolean>) d -> attributeIn("value", webElement).length() > 0);
    }


    public static void waitUntilElementHasNotClass(WebElement webElement, String className) {
        waitUntilElementHasNotClass(webElement, className, 30);
    }

    public static void waitUntilElementHasNotClass(WebElement webElement, String className, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
        wait.until((ExpectedCondition<Boolean>) d -> hasNotClass(className, webElement));
    }


    public static void waitUntilElementHasClass(WebElement webElement, String className) {
        waitUntilElementHasClass(webElement, className, 30);
    }


    public static void waitUntilElementHasClass(WebElement webElement, String className, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
        wait.until((ExpectedCondition<Boolean>) d -> hasClass(className, webElement));
    }


    public static WebElement ancestorOf(WebElement webElement, String xpathOfAncestorElement) {
        return webElement.findElement(By.xpath(String.format(".//ancestor::%s", xpathOfAncestorElement)));
    }

    public static void waitForTextDifferentTo(WebElement webElement, String value, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
        wait.until((ExpectedCondition<Boolean>) d -> !textIn(webElement).equals(value));
    }

    public static void waitForTextDifferentTo(WebElement webElement, String value) {
        waitForTextDifferentTo(webElement, value, 30);
    }

    public static void waitForElementsInList(By by, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(by, 0));
    }

    public static void waitForElementsInList(By by) {
        waitForElementsInList(by, 30);
    }

    public static String getCurrentFrameName() {
        return (String) ((JavascriptExecutor) getDriver()).executeScript("return self.name");
    }

    public static void pressEnter(WebElement webElement) {
        pressKeys(webElement, Keys.ENTER);
    }

    public static void pressKeys(WebElement webElement, CharSequence... keys) {
        webElement.sendKeys(keys);
    }

    public static void select(WebElement webElement) {
        if (isDeselected(webElement)) {
            webElement.click();
        }

    }

    public static void deselect(WebElement webElement) {
        if (isSelected(webElement)) {
            webElement.click();
        }

    }

    public static void selectOption(String text, WebElement webElement) {
        (new Select(webElement)).selectByVisibleText(text);
    }

    public static void deselectOption(String text, WebElement webElement) {
        (new Select(webElement)).deselectByVisibleText(text);
    }

    public static void selectAllOptions(WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var2 = options.iterator();

        while (var2.hasNext()) {
            WebElement option = (WebElement) var2.next();
            select(webElement);
        }

    }

    public static void deselectAllOptions(WebElement webElement) {
        (new Select(webElement)).deselectAll();
    }

    public static void selectOptionWithValue(String value, WebElement webElement) {
        (new Select(webElement)).selectByValue(value);
    }

    public static void deselectOptionWithValue(String value, WebElement webElement) {
        (new Select(webElement)).deselectByValue(value);
    }

    public static void selectOptionWithIndex(int index, WebElement webElement) {
        (new Select(webElement)).selectByIndex(index);
    }

    public static void deselectOptionWithIndex(int index, WebElement webElement) {
        (new Select(webElement)).selectByIndex(index);
    }

    public static void check(WebElement webElement) {
        if (isUnchecked(webElement)) {
            click(webElement);
        }

    }

    public static void uncheck(WebElement webElement) {
        if (isChecked(webElement)) {
            click(webElement);
        }

    }

    public static void open(String url) {
        driver.get(url);
    }

    public static void open(Openable openable, Object... arguments) {
        openable.open(arguments);
    }

    public static void navigateBack() {
        driver.navigate().back();
    }

    public static void navigateForward() {
        driver.navigate().forward();
    }

    public static void navigateRefresh() {
        driver.navigate().refresh();
    }

    public static void waitFor(double seconds) {
        long nanos = (long) (seconds * 1.0E9D);
        if (seconds > 0.0D) {
            try {
                TimeUnit.NANOSECONDS.sleep(nanos);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }

    }

    public static void waitFor(double time, TimeUnit unit) {
        if (time > 0.0D) {
            try {
                TimeUnit.NANOSECONDS.sleep(BotUtils.asNanos(time, unit));
            } catch (InterruptedException var4) {
                var4.printStackTrace();
            }

        }
    }

    public static void waitForElementToBeClickable(WebElement webElement, long secondsToWait) {
        WebDriverWait wait = new WebDriverWait(driver, secondsToWait);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static void waitForElementToDisplay(WebElement webElement) {
        waitForElementToDisplay(webElement, 30L);
    }

    public static void waitIfAnElementIsDisplayed(WebElement webElement, long secondsToWait) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, secondsToWait);
            wait.until(ExpectedConditions.visibilityOf(webElement));
        } catch (NoSuchElementException | TimeoutException e){}
    }

    public static void waitForElementToDisplay(WebElement webElement, long secondsToWait) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, secondsToWait);
            wait.until(ExpectedConditions.visibilityOf(webElement));
        } catch (NoSuchElementException | TimeoutException | StaleElementReferenceException e){
            log.info(e.getMessage());
        }
    }

    public static void waitForElementToDisplayWithoutError(WebComponentList webElement, long secondsToWait) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, secondsToWait);
            wait.until(ExpectedConditions.visibilityOfAllElements(webElement));
        } catch (TimeoutException e){

        }
    }

    public static void waitForElementToBeInvisible(WebElement webElement, long secondsToWait) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, secondsToWait);
            if (webElement.isDisplayed())
                wait.until(ExpectedConditions.invisibilityOf(webElement));

        } catch (NoSuchElementException | TimeoutException | StaleElementReferenceException e) {

        }
    }

    public static void waitForElementToDisplay(WebElement webElement, double timeToWait, TimeUnit unit) {
        WebDriverWait wait = new WebDriverWait(driver, BotUtils.asNanos(timeToWait, unit));
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public static void waitForElementToDisplay(WebElement webElement, long secondsToWait, long sleepInMillis) {
        WebDriverWait wait = new WebDriverWait(driver, secondsToWait, sleepInMillis);
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public static void waitForElementToDisplay(WebElement webElement, double timeToWait, TimeUnit unit, long sleepInMillis) {
        WebDriverWait wait = new WebDriverWait(driver, BotUtils.asNanos(timeToWait, unit), sleepInMillis);
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public static void waitForAttributeClassContains(WebElement webElement, String attributeExpected, long secondsToWait) {
        WebDriverWait wait = new WebDriverWait(driver, secondsToWait);
        wait.until(ExpectedConditions.attributeContains(webElement, "class", attributeExpected));
    }

    public static void waitForListOfElementsVisibles(List<WebElement> elementsExpected, long secondsToWait) {
        WebDriverWait wait = new WebDriverWait(driver, secondsToWait);
        wait.until(ExpectedConditions.visibilityOfAllElements(elementsExpected));
    }

    public static void waitForAttributeToBe(WebElement webElement, String attribute, String value, long secondsToWait){
        try {
            WebDriverWait wait = new WebDriverWait(driver, secondsToWait);
            wait.until(ExpectedConditions.attributeToBe(webElement, attribute, value));
        } catch (Exception e) {
            throw new AssertionError("attribute hasn't change");
        }
    }

    public static Object scrollTo(WebElement webElement) {
        return webElement instanceof WebComponent ? executeJavascript("arguments[0].scrollIntoView(true);", ((WebComponent) webElement).getWrappedWebElement()) : executeJavascript("arguments[0].scrollIntoView(true);", webElement);
    }

    public static void scrollToCenter(WebElement webElement) {
        WebElement target = webElement instanceof WebComponent ? ((WebComponent) webElement).getWrappedWebElement() : webElement;
        String js = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);var elementTop = arguments[0].getBoundingClientRect().top;window.scrollBy(0, elementTop-(viewPortHeight/2));";
        executeJavascript(js, target);
    }

    public static void openInNewTab(WebElement element) {
        type(Keys.chord(new CharSequence[]{BotUtils.getPlatformControlKey(), Keys.RETURN}), element);
    }

    public static Set<String> availableWindowHandles() {
        return driver.getWindowHandles();
    }

    public static String currentWindowHandle() {
        return driver.getWindowHandle();
    }

    public static void switchToWindow(String handle) {
        driver.switchTo().window(handle);
    }

    public static Object executeJavascript(String script, Object... arguments) {
        return ((JavascriptExecutor) getDriver()).executeScript(script, arguments);
    }

    public static Object executeJavascriptAsynchronously(String script, Object... arguments) {
        return ((JavascriptExecutor) getDriver()).executeAsyncScript(script, arguments);
    }

    public static String browser() {
        return ((HasCapabilities) getDriver()).getCapabilities().getBrowserName();
    }

    public static boolean browserIs(String browserName) {
        return StringUtils.equalsIgnoreCase(browser(), browserName);
    }

    public static boolean browserIsNot(String browserName) {
        return !browserIs(browserName);
    }

    public static boolean browserIsAndroid() {
        return browserIs("android");
    }

    public static boolean browserIsNotAndroid() {
        return !browserIsAndroid();
    }

    public static boolean browserIsChrome() {
        return browserIs("chrome") || browserIs("googlechrome");
    }

    public static boolean browserIsNotChrome() {
        return !browserIsChrome();
    }

    public static boolean browserIsEdge() {
        return browserIs("MicrosoftEdge");
    }

    public static boolean browserIsNotEdge() {
        return !browserIsEdge();
    }

    public static boolean browserIsFirefox() {
        return browserIs("firefox");
    }

    public static boolean browserIsNotFirefox() {
        return !browserIsFirefox();
    }

    public static boolean browserIsHtmlUnit() {
        return browserIs("htmlunit");
    }

    public static boolean browserIsNotHtmlUnit() {
        return !browserIsHtmlUnit();
    }

    public static boolean browserIsIPad() {
        return browserIs("iPad");
    }

    public static boolean browserIsNotIPad() {
        return !browserIsIPad();
    }

    public static boolean browserIsIPhone() {
        return browserIs("iPhone");
    }

    public static boolean browserIsNotIPhone() {
        return !browserIsIPhone();
    }

    public static boolean browserIsInternetExplorer() {
        return browserIs("internet explorer") || browserIs("iexplore");
    }

    public static boolean browserIsNotInternetExplorer() {
        return !browserIsInternetExplorer();
    }

    public static boolean browserIsOpera() {
        return browserIs("opera");
    }

    public static boolean browserIsNotOpera() {
        return !browserIsOpera();
    }

    public static boolean browserIsPhantomJS() {
        return browserIs("phantomjs");
    }

    public static boolean browserIsNotPhantomJS() {
        return !browserIsPhantomJS();
    }

    public static boolean browserIsSafari() {
        return browserIs("safari");
    }

    public static boolean browserIsNotSafari() {
        return !browserIsSafari();
    }

    public static String version() {
        return ((HasCapabilities) getDriver()).getCapabilities().getVersion();
    }

    public static boolean versionIs(String version) {
        return StringUtils.equalsIgnoreCase(version(), version);
    }

    public static boolean versionIsNot(String version) {
        return !versionIs(version);
    }

    public static Platform platform() {
        return ((HasCapabilities) getDriver()).getCapabilities().getPlatform();
    }

    public static boolean platformIs(Platform platform) {
        return platform().is(platform);
    }

    public static boolean platformIsNot(Platform platform) {
        return !platformIs(platform);
    }

    public static boolean platformIsAndroid() {
        return platformIs(Platform.ANDROID);
    }

    public static boolean platformIsNotAndroid() {
        return !platformIsAndroid();
    }

    public static boolean platformIsLinux() {
        return platformIs(Platform.LINUX);
    }

    public static boolean platformIsNotLinux() {
        return !platformIsLinux();
    }

    public static boolean platformIsMac() {
        return platformIs(Platform.MAC);
    }

    public static boolean platformIsNotMac() {
        return !platformIsMac();
    }

    public static boolean platformIsUnix() {
        return platformIs(Platform.UNIX);
    }

    public static boolean platformIsNotUnix() {
        return !platformIsUnix();
    }

    public static boolean platformIsWindows() {
        return platformIs(Platform.WINDOWS);
    }

    public static boolean platformIsNotWindows() {
        return !platformIsWindows();
    }

    public static boolean platformIsWin8() {
        return platformIs(Platform.WIN8);
    }

    public static boolean platformIsNotWin8() {
        return !platformIsWin8();
    }

    public static boolean platformIsWin8_1() {
        return platformIs(Platform.WIN8_1);
    }

    public static boolean platformIsNotWin8_1() {
        return !platformIsWin8_1();
    }

    public static boolean platformIsWin10() {
        return platformIs(Platform.WIN10);
    }

    public static boolean platformIsNotWin10() {
        return !platformIsWin10();
    }

    public static boolean platformIsVista() {
        return platformIs(Platform.VISTA);
    }

    public static boolean platformIsNotVista() {
        return !platformIsVista();
    }

    public static boolean platformIsXP() {
        return platformIs(Platform.XP);
    }

    public static boolean platformIsNotXP() {
        return !platformIsXP();
    }

    public static void takeScreenshot(String fileName) {
        File screenshotFile = (File) ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
        String filePath = WebDriverUtils.getScreenshotFilePath(fileName);

        try {
            FileUtils.copyFile(screenshotFile, new File(filePath));
        } catch (IOException var4) {
            throw new WebDriverExtensionException("Failed to save screenshot to " + com.github.webdriverextensions.internal.utils.StringUtils.quote(filePath), var4);
        }
    }

    public static void debug(String str) {
        log.debug(str);
    }

    public static void debug(WebElement webElement) {
        log.debug("Element: " + BotUtils.htmlOf(webElement));
    }

    public static void debug(List<? extends WebElement> webElements) {
        log.debug("Size: ", sizeOf(webElements));
        Iterator var1 = webElements.iterator();

        while (var1.hasNext()) {
            WebElement webElement = (WebElement) var1.next();
            debug(webElement);
        }

    }

    public static boolean isOpen(Openable openable, Object... arguments) {
        return openable.isOpen(arguments);
    }

    public static boolean isNotOpen(Openable openable, Object... arguments) {
        return openable.isNotOpen(arguments);
    }

    public static void assertIsOpen(Openable openable, Object... arguments) {
        openable.assertIsOpen(arguments);
    }

    public static void assertIsNotOpen(Openable openable, Object... arguments) {
        openable.assertIsNotOpen(arguments);
    }

    public static boolean isDisplayed(WebElement webElement) {
        try {
            return webElement.isDisplayed();
        } catch (NoSuchElementException var2) {
            return false;
        }
    }

    public static boolean isNotDisplayed(WebElement webElement) {
        return !isDisplayed(webElement);
    }

    public static boolean isDisplayed(WebElement webElement, long secondsToWait) {
        try {
            WebElement foundWebElement = (WebElement) (new WebDriverWait(driver, secondsToWait)).until(ExpectedConditions.visibilityOf(webElement));
            return foundWebElement != null;
        } catch (RuntimeException var4) {
            return false;
        }
    }

    public static boolean isNotDisplayed(WebElement webElement, long secondsToWait) {
        return !isDisplayed(webElement, secondsToWait);
    }

    public static void assertIsDisplayed(WebElement webElement) {
        if (isNotDisplayed(webElement)) {
            throw new WebAssertionError("Element is not displayed", webElement);
        }
    }

    public static void assertIsNotDisplayed(WebElement webElement) {
        if (isDisplayed(webElement)) {
            throw new WebAssertionError("Element is displayed when it shouldn't", webElement);
        }
    }

    public static void assertIsDisplayed(WebElement webElement, long secondsToWait) {
        if (isNotDisplayed(webElement, secondsToWait)) {
            throw new WebAssertionError("Element is not displayed within " + secondsToWait + " seconds", webElement);
        }
    }

    public static void assertIsNotDisplayed(WebElement webElement, long secondsToWait) {
        if (isDisplayed(webElement, secondsToWait)) {
            throw new WebAssertionError("Element is displayed within " + secondsToWait + " seconds when it shouldn't", webElement);
        }
    }

    public static int sizeOf(Collection collection) {
        return collection.size();
    }

    public static boolean sizeEquals(int number, Collection collection) {
        return BotUtils.isEqual((double) number, (double) collection.size());
    }

    public static boolean sizeNotEquals(int number, Collection collection) {
        return BotUtils.notEquals((double) number, (double) collection.size());
    }

    public static boolean sizeLessThan(int number, Collection collection) {
        return BotUtils.lessThan((double) number, (double) collection.size());
    }

    public static boolean sizeLessThanOrEquals(int number, Collection collection) {
        return BotUtils.lessThanOrEquals((double) number, (double) collection.size());
    }

    public static boolean sizeGreaterThan(int number, Collection collection) {
        return BotUtils.greaterThan((double) number, (double) collection.size());
    }

    public static boolean sizeGreaterThanOrEquals(int number, Collection collection) {
        return BotUtils.greaterThanOrEquals((double) number, (double) collection.size());
    }

    public static void assertSizeEquals(int number, Collection collection) {
        BotUtils.assertEquals("Size", (double) number, (double) collection.size());
    }

    public static void assertSizeNotEquals(int number, Collection collection) {
        BotUtils.assertNotEquals("Size", (double) number, (double) collection.size());
    }

    public static void assertSizeLessThan(int number, Collection collection) {
        BotUtils.assertLessThan("Size", (double) number, (double) collection.size());
    }

    public static void assertSizeLessThanOrEquals(int number, Collection collection) {
        BotUtils.assertLessThanOrEquals("Size", (double) number, (double) collection.size());
    }

    public static void assertSizeGreaterThan(int number, Collection collection) {
        BotUtils.assertGreaterThan("Size", (double) number, (double) collection.size());
    }

    public static void assertSizeGreaterThanOrEquals(int number, Collection collection) {
        BotUtils.assertGreaterThanOrEquals("Size", (double) number, (double) collection.size());
    }

    public static String currentUrl() {
        return driver.getCurrentUrl();
    }

    public static boolean currentUrlEquals(String url) {
        return BotUtils.isEqual(url, currentUrl());
    }

    public static boolean currentUrlNotEquals(String url) {
        return BotUtils.notEquals(url, currentUrl());
    }

    public static boolean currentUrlContains(String searchText) {
        return BotUtils.contains(searchText, currentUrl());
    }

    public static boolean currentUrlNotContains(String searchText) {
        return BotUtils.notContains(searchText, currentUrl());
    }

    public static boolean currentUrlStartsWith(String prefix) {
        return BotUtils.startsWith(prefix, currentUrl());
    }

    public static boolean currentUrlNotStartsWith(String prefix) {
        return BotUtils.notStartsWith(prefix, currentUrl());
    }

    public static boolean currentUrlEndsWith(String suffix) {
        return BotUtils.endsWith(suffix, currentUrl());
    }

    public static boolean currentUrlNotEndsWith(String suffix) {
        return BotUtils.notEndsWith(suffix, currentUrl());
    }

    public static boolean currentUrlMatches(String regExp) {
        return BotUtils.matches(regExp, currentUrl());
    }

    public static boolean currentUrlNotMatches(String regExp) {
        return BotUtils.notMatches(regExp, currentUrl());
    }

    public static void assertCurrentUrlEquals(String url) {
        BotUtils.assertEquals("Current url", url, currentUrl());
    }

    public static void assertCurrentUrlNotEquals(String url) {
        BotUtils.assertNotEquals("Current url", url, currentUrl());
    }

    public static void assertCurrentUrlContains(String searchText) {
        BotUtils.assertContains("Current url", searchText, currentUrl());
    }

    public static void assertCurrentUrlNotContains(String searchText) {
        BotUtils.assertNotContains("Current url", searchText, currentUrl());
    }

    public static void assertCurrentUrlStartsWith(String prefix) {
        BotUtils.assertStartsWith("Current url", prefix, currentUrl());
    }

    public static void assertCurrentUrlNotStartsWith(String prefix) {
        BotUtils.assertNotStartsWith("Current url", prefix, currentUrl());
    }

    public static void assertCurrentUrlEndsWith(String suffix) {
        BotUtils.assertEndsWith("Current url", suffix, currentUrl());
    }

    public static void assertCurrentUrlNotEndsWith(String suffix) {
        BotUtils.assertNotEndsWith("Current url", suffix, currentUrl());
    }

    public static void assertCurrentUrlMatches(String regExp) {
        BotUtils.assertMatches("Current url", regExp, currentUrl());
    }

    public static void assertCurrentUrlNotMatches(String regExp) {
        BotUtils.assertNotMatches("Current url", regExp, currentUrl());
    }

    public static String title() {
        return driver.getTitle();
    }

    public static boolean titleEquals(String title) {
        return BotUtils.isEqual(title, title());
    }

    public static boolean titleNotEquals(String title) {
        return BotUtils.notEquals(title, title());
    }

    public static boolean titleContains(String searchText) {
        return BotUtils.contains(searchText, title());
    }

    public static boolean titleNotContains(String searchText) {
        return BotUtils.notContains(searchText, title());
    }

    public static boolean titleStartsWith(String prefix) {
        return BotUtils.startsWith(prefix, title());
    }

    public static boolean titleNotStartsWith(String prefix) {
        return BotUtils.notStartsWith(prefix, title());
    }

    public static boolean titleEndsWith(String suffix) {
        return BotUtils.endsWith(suffix, title());
    }

    public static boolean titleNotEndsWith(String suffix) {
        return BotUtils.notEndsWith(suffix, title());
    }

    public static boolean titleMatches(String regExp) {
        return BotUtils.matches(regExp, title());
    }

    public static boolean titleNotMatches(String regExp) {
        return BotUtils.notMatches(regExp, title());
    }

    public static void assertTitleEquals(String title) {
        BotUtils.assertEquals("Title", title, title());
    }

    public static void assertTitleNotEquals(String title) {
        BotUtils.assertNotEquals("Title", title, title());
    }

    public static void assertTitleContains(String searchText) {
        BotUtils.assertContains("Title", searchText, title());
    }

    public static void assertTitleNotContains(String searchText) {
        BotUtils.assertNotContains("Title", searchText, title());
    }

    public static void assertTitleStartsWith(String prefix) {
        BotUtils.assertStartsWith("Title", prefix, title());
    }

    public static void assertTitleNotStartsWith(String prefix) {
        BotUtils.assertNotStartsWith("Title", prefix, title());
    }

    public static void assertTitleEndsWith(String suffix) {
        BotUtils.assertEndsWith("Title", suffix, title());
    }

    public static void assertTitleNotEndsWith(String suffix) {
        BotUtils.assertNotEndsWith("Title", suffix, title());
    }

    public static void assertTitleMatches(String regExp) {
        BotUtils.assertMatches("Title", regExp, title());
    }

    public static void assertTitleNotMatches(String regExp) {
        BotUtils.assertNotMatches("Title", regExp, title());
    }

    public static String tagNameOf(WebElement webElement) {
        return webElement.getTagName();
    }

    public static boolean tagNameEquals(String value, WebElement webElement) {
        return BotUtils.isEqual(value, tagNameOf(webElement));
    }

    public static boolean tagNameNotEquals(String value, WebElement webElement) {
        return BotUtils.notEquals(value, tagNameOf(webElement));
    }

    public static void assertTagNameEquals(String value, WebElement webElement) {
        BotUtils.assertEquals("Tag name", value, tagNameOf(webElement), webElement);
    }

    public static void assertTagNameNotEquals(String value, WebElement webElement) {
        BotUtils.assertNotEquals("Tag name", value, tagNameOf(webElement), webElement);
    }

    public static String attributeIn(String name, WebElement webElement) {
        return webElement.getAttribute(name);
    }

    public static boolean hasAttribute(String name, WebElement webElement) {
        return webElement.getAttribute(name) != null;
    }

    public static boolean hasNotAttribute(String name, WebElement webElement) {
        return !hasAttribute(name, webElement);
    }

    public static boolean attributeEquals(String name, String value, WebElement webElement) {
        return BotUtils.isEqual(value, attributeIn(name, webElement));
    }

    public static boolean attributeNotEquals(String name, String value, WebElement webElement) {
        return BotUtils.notEquals(value, attributeIn(name, webElement));
    }

    public static boolean attributeContains(String name, String searchText, WebElement webElement) {
        return BotUtils.contains(searchText, attributeIn(name, webElement));
    }

    public static boolean attributeNotContains(String name, String searchText, WebElement webElement) {
        return BotUtils.notContains(searchText, attributeIn(name, webElement));
    }

    public static boolean attributeStartsWith(String name, String prefix, WebElement webElement) {
        return BotUtils.startsWith(prefix, attributeIn(name, webElement));
    }

    public static boolean attributeNotStartsWith(String name, String prefix, WebElement webElement) {
        return BotUtils.notStartsWith(prefix, attributeIn(name, webElement));
    }

    public static boolean attributeEndsWith(String name, String suffix, WebElement webElement) {
        return BotUtils.endsWith(suffix, attributeIn(name, webElement));
    }

    public static boolean attributeNotEndsWith(String name, String suffix, WebElement webElement) {
        return BotUtils.notEndsWith(suffix, attributeIn(name, webElement));
    }

    public static boolean attributeMatches(String name, String regExp, WebElement webElement) {
        return BotUtils.matches(regExp, attributeIn(name, webElement));
    }

    public static boolean attributeNotMatches(String name, String regExp, WebElement webElement) {
        return BotUtils.notMatches(regExp, attributeIn(name, webElement));
    }

    public static void assertHasAttribute(String name, WebElement webElement) {
        if (hasNotAttribute(name, webElement)) {
            throw new WebAssertionError("Element does not have attribute " + com.github.webdriverextensions.internal.utils.StringUtils.quote(name), webElement);
        }
    }

    public static void assertHasNotAttribute(String name, WebElement webElement) {
        if (hasAttribute(name, webElement)) {
            throw new WebAssertionError("Element has attribute " + com.github.webdriverextensions.internal.utils.StringUtils.quote(name) + " when it shouldn't", webElement);
        }
    }

    public static void assertAttributeEquals(String name, String value, WebElement webElement) {
        BotUtils.assertEquals("Element attribute " + name, value, attributeIn(name, webElement), webElement);
    }

    public static void assertAttributeNotEquals(String name, String value, WebElement webElement) {
        BotUtils.assertNotEquals("Element attribute " + name, value, attributeIn(name, webElement), webElement);
    }

    public static void assertAttributeContains(String name, String searchText, WebElement webElement) {
        BotUtils.assertContains("Element attribute " + name, searchText, attributeIn(name, webElement), webElement);
    }

    public static void assertAttributeNotContains(String name, String searchText, WebElement webElement) {
        BotUtils.assertNotContains("Element attribute " + name, searchText, attributeIn(name, webElement), webElement);
    }

    public static void assertAttributeStartsWith(String name, String prefix, WebElement webElement) {
        BotUtils.assertStartsWith("Element attribute " + name, prefix, attributeIn(name, webElement), webElement);
    }

    public static void assertAttributeNotStartsWith(String name, String prefix, WebElement webElement) {
        BotUtils.assertNotStartsWith("Element attribute " + name, prefix, attributeIn(name, webElement), webElement);
    }

    public static void assertAttributeEndsWith(String name, String suffix, WebElement webElement) {
        BotUtils.assertEndsWith("Element attribute " + name, suffix, attributeIn(name, webElement), webElement);
    }

    public static void assertAttributeNotEndsWith(String name, String suffix, WebElement webElement) {
        BotUtils.assertNotEndsWith("Element attribute " + name, suffix, attributeIn(name, webElement), webElement);
    }

    public static void assertAttributeMatches(String name, String regExp, WebElement webElement) {
        BotUtils.assertMatches("Element attribute " + name, regExp, attributeIn(name, webElement), webElement);
    }

    public static void assertAttributeNotMatches(String name, String regExp, WebElement webElement) {
        BotUtils.assertNotMatches("Element attribute " + name, regExp, attributeIn(name, webElement), webElement);
    }

    public static double attributeInAsNumber(String name, WebElement webElement) {
        return org.apache.commons.lang3.math.NumberUtils.createDouble(attributeIn(name, webElement));
    }

    public static boolean attributeIsNumber(String name, WebElement webElement) {
        try {
            attributeInAsNumber(name, webElement);
            return true;
        } catch (NumberFormatException var3) {
            return false;
        }
    }

    public static boolean attributeIsNotNumber(String name, WebElement webElement) {
        return !attributeIsNumber(name, webElement);
    }

    public static boolean attributeEquals(String name, double number, WebElement webElement) {
        return BotUtils.isEqual(number, attributeInAsNumber(name, webElement));
    }

    public static boolean attributeNotEquals(String name, double number, WebElement webElement) {
        return BotUtils.notEquals(number, attributeInAsNumber(name, webElement));
    }

    public static boolean attributeLessThan(String name, double number, WebElement webElement) {
        return BotUtils.lessThan(number, attributeInAsNumber(name, webElement));
    }

    public static boolean attributeLessThanOrEquals(String name, double number, WebElement webElement) {
        return BotUtils.lessThanOrEquals(number, attributeInAsNumber(name, webElement));
    }

    public static boolean attributeGreaterThan(String name, double number, WebElement webElement) {
        return BotUtils.greaterThan(number, attributeInAsNumber(name, webElement));
    }

    public static boolean attributeGreaterThanOrEquals(String name, double number, WebElement webElement) {
        return BotUtils.greaterThanOrEquals(number, attributeInAsNumber(name, webElement));
    }

    public static void assertAttributeIsNumber(String name, WebElement webElement) {
        if (attributeIsNotNumber(name, webElement)) {
            throw new WebAssertionError("Element attribute " + name + " is not a number", webElement);
        }
    }

    public static void assertAttributeIsNotNumber(String name, WebElement webElement) {
        if (attributeIsNumber(name, webElement)) {
            throw new WebAssertionError("Element attribute " + name + " is a number when it shouldn't", webElement);
        }
    }

    public static void assertAttributeEquals(String name, double number, WebElement webElement) {
        BotUtils.assertEquals(name, number, attributeInAsNumber(name, webElement), webElement);
    }

    public static void assertAttributeNotEquals(String name, double number, WebElement webElement) {
        BotUtils.assertNotEquals(name, number, attributeInAsNumber(name, webElement), webElement);
    }

    public static void assertAttributeLessThan(String name, double number, WebElement webElement) {
        BotUtils.assertLessThan(name, number, attributeInAsNumber(name, webElement), webElement);
    }

    public static void assertAttributeLessThanOrEquals(String name, double number, WebElement webElement) {
        BotUtils.assertLessThanOrEquals(name, number, attributeInAsNumber(name, webElement), webElement);
    }

    public static void assertAttributeGreaterThan(String name, double number, WebElement webElement) {
        BotUtils.assertGreaterThan(name, number, attributeInAsNumber(name, webElement), webElement);
    }

    public static void assertAttributeGreaterThanOrEquals(String name, double number, WebElement webElement) {
        BotUtils.assertGreaterThanOrEquals(name, number, attributeInAsNumber(name, webElement), webElement);
    }

    public static String idIn(WebElement webElement) {
        return attributeIn("id", webElement);
    }

    public static boolean hasId(WebElement webElement) {
        return hasAttribute("id", webElement);
    }

    public static boolean hasNotId(WebElement webElement) {
        return hasNotAttribute("id", webElement);
    }

    public static boolean idEquals(String value, WebElement webElement) {
        return attributeEquals("id", value, webElement);
    }

    public static boolean idNotEquals(String value, WebElement webElement) {
        return attributeNotEquals("id", value, webElement);
    }

    public static boolean idContains(String searchText, WebElement webElement) {
        return attributeContains("id", searchText, webElement);
    }

    public static boolean idNotContains(String searchText, WebElement webElement) {
        return attributeNotContains("id", searchText, webElement);
    }

    public static boolean idStartsWith(String prefix, WebElement webElement) {
        return attributeStartsWith("id", prefix, webElement);
    }

    public static boolean idNotStartsWith(String prefix, WebElement webElement) {
        return attributeNotStartsWith("id", prefix, webElement);
    }

    public static boolean idEndsWith(String suffix, WebElement webElement) {
        return attributeEndsWith("id", suffix, webElement);
    }

    public static boolean idNotEndsWith(String suffix, WebElement webElement) {
        return attributeNotEndsWith("id", suffix, webElement);
    }

    public static boolean idMatches(String regExp, WebElement webElement) {
        return attributeMatches("id", regExp, webElement);
    }

    public static boolean idNotMatches(String regExp, WebElement webElement) {
        return attributeNotMatches("id", regExp, webElement);
    }

    public static void assertHasId(WebElement webElement) {
        assertHasAttribute("id", webElement);
    }

    public static void assertHasNotId(WebElement webElement) {
        assertHasNotAttribute("id", webElement);
    }

    public static void assertIdEquals(String value, WebElement webElement) {
        assertAttributeEquals("id", value, webElement);
    }

    public static void assertIdNotEquals(String value, WebElement webElement) {
        assertAttributeNotEquals("id", value, webElement);
    }

    public static void assertIdContains(String searchText, WebElement webElement) {
        assertAttributeContains("id", searchText, webElement);
    }

    public static void assertIdNotContains(String searchText, WebElement webElement) {
        assertAttributeNotContains("id", searchText, webElement);
    }

    public static void assertIdStartsWith(String prefix, WebElement webElement) {
        assertAttributeStartsWith("id", prefix, webElement);
    }

    public static void assertIdNotStartsWith(String prefix, WebElement webElement) {
        assertAttributeNotStartsWith("id", prefix, webElement);
    }

    public static void assertIdEndsWith(String suffix, WebElement webElement) {
        assertAttributeEndsWith("id", suffix, webElement);
    }

    public static void assertIdNotEndsWith(String suffix, WebElement webElement) {
        assertAttributeNotEndsWith("id", suffix, webElement);
    }

    public static void assertIdMatches(String regExp, WebElement webElement) {
        assertAttributeMatches("id", regExp, webElement);
    }

    public static void assertIdNotMatches(String regExp, WebElement webElement) {
        assertAttributeNotMatches("id", regExp, webElement);
    }

    public static String nameIn(WebElement webElement) {
        return attributeIn("name", webElement);
    }

    public static boolean hasName(WebElement webElement) {
        return hasAttribute("name", webElement);
    }

    public static boolean hasNotName(WebElement webElement) {
        return hasNotAttribute("name", webElement);
    }

    public static boolean nameEquals(String value, WebElement webElement) {
        return attributeEquals("name", value, webElement);
    }

    public static boolean nameNotEquals(String value, WebElement webElement) {
        return attributeNotEquals("name", value, webElement);
    }

    public static boolean nameContains(String searchText, WebElement webElement) {
        return attributeContains("name", searchText, webElement);
    }

    public static boolean nameNotContains(String searchText, WebElement webElement) {
        return attributeNotContains("name", searchText, webElement);
    }

    public static boolean nameStartsWith(String prefix, WebElement webElement) {
        return attributeStartsWith("name", prefix, webElement);
    }

    public static boolean nameNotStartsWith(String prefix, WebElement webElement) {
        return attributeNotStartsWith("name", prefix, webElement);
    }

    public static boolean nameEndsWith(String suffix, WebElement webElement) {
        return attributeEndsWith("name", suffix, webElement);
    }

    public static boolean nameNotEndsWith(String suffix, WebElement webElement) {
        return attributeNotEndsWith("name", suffix, webElement);
    }

    public static boolean nameMatches(String regExp, WebElement webElement) {
        return attributeMatches("name", regExp, webElement);
    }

    public static boolean nameNotMatches(String regExp, WebElement webElement) {
        return attributeNotMatches("name", regExp, webElement);
    }

    public static void assertHasName(WebElement webElement) {
        assertHasAttribute("name", webElement);
    }

    public static void assertHasNotName(WebElement webElement) {
        assertHasNotAttribute("name", webElement);
    }

    public static void assertNameEquals(String value, WebElement webElement) {
        assertAttributeEquals("name", value, webElement);
    }

    public static void assertNameNotEquals(String value, WebElement webElement) {
        assertAttributeNotEquals("name", value, webElement);
    }

    public static void assertNameContains(String searchText, WebElement webElement) {
        assertAttributeContains("name", searchText, webElement);
    }

    public static void assertNameNotContains(String searchText, WebElement webElement) {
        assertAttributeNotContains("name", searchText, webElement);
    }

    public static void assertNameStartsWith(String prefix, WebElement webElement) {
        assertAttributeStartsWith("name", prefix, webElement);
    }

    public static void assertNameNotStartsWith(String prefix, WebElement webElement) {
        assertAttributeNotStartsWith("name", prefix, webElement);
    }

    public static void assertNameEndsWith(String suffix, WebElement webElement) {
        assertAttributeEndsWith("name", suffix, webElement);
    }

    public static void assertNameNotEndsWith(String suffix, WebElement webElement) {
        assertAttributeNotEndsWith("name", suffix, webElement);
    }

    public static void assertNameMatches(String regExp, WebElement webElement) {
        assertAttributeMatches("name", regExp, webElement);
    }

    public static void assertNameNotMatches(String regExp, WebElement webElement) {
        assertAttributeNotMatches("name", regExp, webElement);
    }

    public static String classIn(WebElement webElement) {
        return attributeIn("class", webElement);
    }

    public static List<String> classesIn(WebElement webElement) {
        return Arrays.asList(StringUtils.split(classIn(webElement)));
    }

    public static boolean hasClass(WebElement webElement) {
        return hasAttribute("class", webElement);
    }

    public static boolean hasNotClass(WebElement webElement) {
        return hasNotAttribute("class", webElement);
    }

    public static boolean hasClass(String className, WebElement webElement) {
        List<String> classes = classesIn(webElement);
        Iterator var3 = classes.iterator();

        String clazz;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            clazz = (String) var3.next();
        } while (!BotUtils.isEqual(className, clazz));

        return true;
    }

    public static boolean hasNotClass(String className, WebElement webElement) {
        return !hasClass(className, webElement);
    }

    public static boolean hasClassContaining(String searchText, WebElement webElement) {
        List<String> classes = classesIn(webElement);
        Iterator var3 = classes.iterator();

        String clazz;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            clazz = (String) var3.next();
        } while (!BotUtils.contains(searchText, clazz));

        return true;
    }

    public static boolean hasNotClassContaining(String searchText, WebElement webElement) {
        return !hasClassContaining(searchText, webElement);
    }

    public static boolean hasClassStartingWith(String prefix, WebElement webElement) {
        List<String> classes = classesIn(webElement);
        Iterator var3 = classes.iterator();

        String clazz;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            clazz = (String) var3.next();
        } while (!BotUtils.startsWith(prefix, clazz));

        return true;
    }

    public static boolean hasNotClassStartingWith(String prefix, WebElement webElement) {
        return !hasClassStartingWith(prefix, webElement);
    }

    public static boolean hasClassEndingWith(String suffix, WebElement webElement) {
        List<String> classes = classesIn(webElement);
        Iterator var3 = classes.iterator();

        String clazz;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            clazz = (String) var3.next();
        } while (!BotUtils.endsWith(suffix, clazz));

        return true;
    }

    public static boolean hasNotClassEndingWith(String suffix, WebElement webElement) {
        return !hasClassEndingWith(suffix, webElement);
    }

    public static boolean hasClassMatching(String regExp, WebElement webElement) {
        List<String> classes = classesIn(webElement);
        Iterator var3 = classes.iterator();

        String clazz;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            clazz = (String) var3.next();
        } while (!BotUtils.matches(regExp, clazz));

        return true;
    }

    public static boolean hasNotClassMatching(String regExp, WebElement webElement) {
        return !hasClassMatching(regExp, webElement);
    }

    public static void assertHasClass(WebElement webElement) {
        assertHasAttribute("class", webElement);
    }

    public static void assertHasNotClass(WebElement webElement) {
        assertHasNotAttribute("class", webElement);
    }

    public static void assertHasClass(String className, WebElement webElement) {
        if (hasNotClass(className, webElement)) {
            throw new WebAssertionError("Element does not have class " + com.github.webdriverextensions.internal.utils.StringUtils.quote(className.trim()), webElement);
        }
    }

    public static void assertHasNotClass(String className, WebElement webElement) {
        if (hasClass(className, webElement)) {
            throw new WebAssertionError("Element has class " + com.github.webdriverextensions.internal.utils.StringUtils.quote(className.trim()) + " when it shouldn't", webElement);
        }
    }

    public static void assertHasClassContaining(String searchText, WebElement webElement) {
        if (hasNotClassContaining(searchText, webElement)) {
            throw new WebAssertionError("Element does not have class containing text " + com.github.webdriverextensions.internal.utils.StringUtils.quote(searchText.trim()), webElement);
        }
    }

    public static void assertHasNotClassContaining(String searchText, WebElement webElement) {
        if (hasClassContaining(searchText, webElement)) {
            throw new WebAssertionError("Element has class containing text " + com.github.webdriverextensions.internal.utils.StringUtils.quote(searchText.trim()) + " when it shouldn't", webElement);
        }
    }

    public static void assertHasClassStartingWith(String prefix, WebElement webElement) {
        if (hasNotClassStartingWith(prefix, webElement)) {
            throw new WebAssertionError("Element does not have class containing prefix " + com.github.webdriverextensions.internal.utils.StringUtils.quote(prefix.trim()), webElement);
        }
    }

    public static void assertHasNotClassStartingWith(String prefix, WebElement webElement) {
        if (hasClassStartingWith(prefix, webElement)) {
            throw new WebAssertionError("Element has class containing prefix " + com.github.webdriverextensions.internal.utils.StringUtils.quote(prefix.trim()) + " when it shouldn't", webElement);
        }
    }

    public static void assertHasClassEndingWith(String suffix, WebElement webElement) {
        if (hasNotClassEndingWith(suffix, webElement)) {
            throw new WebAssertionError("Element does not have class containing suffix " + com.github.webdriverextensions.internal.utils.StringUtils.quote(suffix.trim()), webElement);
        }
    }

    public static void assertHasNotClassEndingWith(String suffix, WebElement webElement) {
        if (hasClassEndingWith(suffix, webElement)) {
            throw new WebAssertionError("Element has class containing suffix " + com.github.webdriverextensions.internal.utils.StringUtils.quote(suffix.trim()) + " when it shouldn't", webElement);
        }
    }

    public static void assertHasClassMatching(String regExp, WebElement webElement) {
        if (hasNotClassMatching(regExp, webElement)) {
            throw new WebAssertionError("Element does not have class matching regExp " + com.github.webdriverextensions.internal.utils.StringUtils.quote(regExp.trim()), webElement);
        }
    }

    public static void assertHasNotClassMatching(String regExp, WebElement webElement) {
        if (hasClassMatching(regExp, webElement)) {
            throw new WebAssertionError("Element has class matching regExp " + com.github.webdriverextensions.internal.utils.StringUtils.quote(regExp.trim()) + " when it shouldn't", webElement);
        }
    }

    public static String valueIn(WebElement webElement) {
        return attributeIn("value", webElement);
    }

    public static boolean hasValue(WebElement webElement) {
        return hasAttribute("value", webElement);
    }

    public static boolean hasNotValue(WebElement webElement) {
        return hasNotAttribute("value", webElement);
    }

    public static boolean valueEquals(String value, WebElement webElement) {
        return attributeEquals("value", value, webElement);
    }

    public static boolean valueNotEquals(String value, WebElement webElement) {
        return attributeNotEquals("value", value, webElement);
    }

    public static boolean valueContains(String searchText, WebElement webElement) {
        return attributeContains("value", searchText, webElement);
    }

    public static boolean valueNotContains(String searchText, WebElement webElement) {
        return attributeNotContains("value", searchText, webElement);
    }

    public static boolean valueStartsWith(String prefix, WebElement webElement) {
        return attributeStartsWith("value", prefix, webElement);
    }

    public static boolean valueNotStartsWith(String prefix, WebElement webElement) {
        return attributeNotStartsWith("value", prefix, webElement);
    }

    public static boolean valueEndsWith(String suffix, WebElement webElement) {
        return attributeEndsWith("value", suffix, webElement);
    }

    public static boolean valueNotEndsWith(String suffix, WebElement webElement) {
        return attributeNotEndsWith("value", suffix, webElement);
    }

    public static boolean valueMatches(String regExp, WebElement webElement) {
        return attributeMatches("value", regExp, webElement);
    }

    public static boolean valueNotMatches(String regExp, WebElement webElement) {
        return attributeNotMatches("value", regExp, webElement);
    }

    public static void assertHasValue(WebElement webElement) {
        assertHasAttribute("value", webElement);
    }

    public static void assertHasNotValue(WebElement webElement) {
        assertHasNotAttribute("value", webElement);
    }

    public static void assertValueEquals(String value, WebElement webElement) {
        assertAttributeEquals("value", value, webElement);
    }

    public static void assertValueNotEquals(String value, WebElement webElement) {
        assertAttributeNotEquals("value", value, webElement);
    }

    public static void assertValueContains(String searchText, WebElement webElement) {
        assertAttributeContains("value", searchText, webElement);
    }

    public static void assertValueNotContains(String searchText, WebElement webElement) {
        assertAttributeNotContains("value", searchText, webElement);
    }

    public static void assertValueStartsWith(String prefix, WebElement webElement) {
        assertAttributeStartsWith("value", prefix, webElement);
    }

    public static void assertValueNotStartsWith(String prefix, WebElement webElement) {
        assertAttributeNotStartsWith("value", prefix, webElement);
    }

    public static void assertValueEndsWith(String suffix, WebElement webElement) {
        assertAttributeEndsWith("value", suffix, webElement);
    }

    public static void assertValueNotEndsWith(String suffix, WebElement webElement) {
        assertAttributeNotEndsWith("value", suffix, webElement);
    }

    public static void assertValueMatches(String regExp, WebElement webElement) {
        assertAttributeMatches("value", regExp, webElement);
    }

    public static void assertValueNotMatches(String regExp, WebElement webElement) {
        assertAttributeNotMatches("value", regExp, webElement);
    }

    public static double valueInAsNumber(WebElement webElement) {
        return attributeInAsNumber("value", webElement);
    }

    public static boolean valueIsNumber(WebElement webElement) {
        return attributeIsNumber("value", webElement);
    }

    public static boolean valueIsNotNumber(WebElement webElement) {
        return attributeIsNotNumber("value", webElement);
    }

    public static boolean valueEquals(double number, WebElement webElement) {
        return attributeEquals("value", number, webElement);
    }

    public static boolean valueNotEquals(double number, WebElement webElement) {
        return attributeNotEquals("value", number, webElement);
    }

    public static boolean valueLessThan(double number, WebElement webElement) {
        return attributeLessThan("value", number, webElement);
    }

    public static boolean valueLessThanOrEquals(double number, WebElement webElement) {
        return attributeLessThanOrEquals("value", number, webElement);
    }

    public static boolean valueGreaterThan(double number, WebElement webElement) {
        return attributeGreaterThan("value", number, webElement);
    }

    public static boolean valueGreaterThanOrEquals(double number, WebElement webElement) {
        return attributeGreaterThanOrEquals("value", number, webElement);
    }

    public static void assertValueIsNumber(WebElement webElement) {
        assertAttributeIsNumber("value", webElement);
    }

    public static void assertValueIsNotNumber(WebElement webElement) {
        assertAttributeIsNotNumber("value", webElement);
    }

    public static void assertValueEquals(double number, WebElement webElement) {
        assertAttributeEquals("value", number, webElement);
    }

    public static void assertValueNotEquals(double number, WebElement webElement) {
        assertAttributeNotEquals("value", number, webElement);
    }

    public static void assertValueLessThan(double number, WebElement webElement) {
        assertAttributeLessThan("value", number, webElement);
    }

    public static void assertValueLessThanOrEquals(double number, WebElement webElement) {
        assertAttributeLessThanOrEquals("value", number, webElement);
    }

    public static void assertValueGreaterThan(double number, WebElement webElement) {
        assertAttributeGreaterThan("value", number, webElement);
    }

    public static void assertValueGreaterThanOrEquals(double number, WebElement webElement) {
        assertAttributeGreaterThanOrEquals("value", number, webElement);
    }

    public static String hrefIn(WebElement webElement) {
        return attributeIn("href", webElement);
    }

    public static boolean hasHref(WebElement webElement) {
        return hasAttribute("href", webElement);
    }

    public static boolean hasNotHref(WebElement webElement) {
        return hasNotAttribute("href", webElement);
    }

    public static boolean hrefEquals(String value, WebElement webElement) {
        return attributeEquals("href", value, webElement);
    }

    public static boolean hrefNotEquals(String value, WebElement webElement) {
        return attributeNotEquals("href", value, webElement);
    }

    public static boolean hrefContains(String searchText, WebElement webElement) {
        return attributeContains("href", searchText, webElement);
    }

    public static boolean hrefNotContains(String searchText, WebElement webElement) {
        return attributeNotContains("href", searchText, webElement);
    }

    public static boolean hrefStartsWith(String prefix, WebElement webElement) {
        return attributeStartsWith("href", prefix, webElement);
    }

    public static boolean hrefNotStartsWith(String prefix, WebElement webElement) {
        return attributeNotStartsWith("href", prefix, webElement);
    }

    public static boolean hrefEndsWith(String suffix, WebElement webElement) {
        return attributeEndsWith("href", suffix, webElement);
    }

    public static boolean hrefNotEndsWith(String suffix, WebElement webElement) {
        return attributeNotEndsWith("href", suffix, webElement);
    }

    public static boolean hrefMatches(String regExp, WebElement webElement) {
        return attributeMatches("href", regExp, webElement);
    }

    public static boolean hrefNotMatches(String regExp, WebElement webElement) {
        return attributeNotMatches("href", regExp, webElement);
    }

    public static void assertHasHref(WebElement webElement) {
        assertHasAttribute("href", webElement);
    }

    public static void assertHasNotHref(WebElement webElement) {
        assertHasNotAttribute("href", webElement);
    }

    public static void assertHrefEquals(String value, WebElement webElement) {
        assertAttributeEquals("href", value, webElement);
    }

    public static void assertHrefNotEquals(String value, WebElement webElement) {
        assertAttributeNotEquals("href", value, webElement);
    }

    public static void assertHrefContains(String searchText, WebElement webElement) {
        assertAttributeContains("href", searchText, webElement);
    }

    public static void assertHrefNotContains(String searchText, WebElement webElement) {
        assertAttributeNotContains("href", searchText, webElement);
    }

    public static void assertHrefStartsWith(String prefix, WebElement webElement) {
        assertAttributeStartsWith("href", prefix, webElement);
    }

    public static void assertHrefNotStartsWith(String prefix, WebElement webElement) {
        assertAttributeNotStartsWith("href", prefix, webElement);
    }

    public static void assertHrefEndsWith(String suffix, WebElement webElement) {
        assertAttributeEndsWith("href", suffix, webElement);
    }

    public static void assertHrefNotEndsWith(String suffix, WebElement webElement) {
        assertAttributeNotEndsWith("href", suffix, webElement);
    }

    public static void assertHrefMatches(String regExp, WebElement webElement) {
        assertAttributeMatches("href", regExp, webElement);
    }

    public static void assertHrefNotMatches(String regExp, WebElement webElement) {
        assertAttributeNotMatches("href", regExp, webElement);
    }

    public static String textIn(WebElement webElement) {
        return StringUtils.trim(webElement.getText());
    }

    public static boolean hasText(WebElement webElement) {
        return BotUtils.notEquals("", textIn(webElement));
    }

    public static boolean hasNotText(WebElement webElement) {
        return BotUtils.isEqual("", textIn(webElement));
    }

    public static boolean textEquals(String text, WebElement webElement) {
        return BotUtils.isEqual(text, textIn(webElement));
    }

    public static boolean textNotEquals(String text, WebElement webElement) {
        return BotUtils.notEquals(text, textIn(webElement));
    }

    public static boolean textEqualsIgnoreCase(String text, WebElement webElement) {
        return BotUtils.equalsIgnoreCase(text, textIn(webElement));
    }

    public static boolean textNotEqualsIgnoreCase(String text, WebElement webElement) {
        return BotUtils.notEqualsIgnoreCase(text, textIn(webElement));
    }

    public static boolean textContains(String searchText, WebElement webElement) {
        return BotUtils.contains(searchText, textIn(webElement));
    }

    public static boolean textNotContains(String searchText, WebElement webElement) {
        return BotUtils.notContains(searchText, textIn(webElement));
    }

    public static boolean textContainsIgnoreCase(String searchText, WebElement webElement) {
        return BotUtils.containsIgnoreCase(searchText, textIn(webElement));
    }

    public static boolean textNotContainsIgnoreCase(String searchText, WebElement webElement) {
        return BotUtils.notContainsIgnoreCase(searchText, textIn(webElement));
    }

    public static boolean textStartsWith(String prefix, WebElement webElement) {
        return BotUtils.startsWith(prefix, textIn(webElement));
    }

    public static boolean textNotStartsWith(String prefix, WebElement webElement) {
        return BotUtils.notStartsWith(prefix, textIn(webElement));
    }

    public static boolean textStartsWithIgnoreCase(String prefix, WebElement webElement) {
        return BotUtils.startsWithIgnoreCase(prefix, textIn(webElement));
    }

    public static boolean textNotStartsWithIgnoreCase(String prefix, WebElement webElement) {
        return BotUtils.notStartsWithIgnoreCase(prefix, textIn(webElement));
    }

    public static boolean textEndsWith(String suffix, WebElement webElement) {
        return BotUtils.endsWith(suffix, textIn(webElement));
    }

    public static boolean textNotEndsWith(String suffix, WebElement webElement) {
        return BotUtils.notEndsWith(suffix, textIn(webElement));
    }

    public static boolean textEndsWithIgnoreCase(String suffix, WebElement webElement) {
        return BotUtils.endsWithIgnoreCase(suffix, textIn(webElement));
    }

    public static boolean textNotEndsWithIgnoreCase(String suffix, WebElement webElement) {
        return BotUtils.notEndsWithIgnoreCase(suffix, textIn(webElement));
    }

    public static boolean textMatches(String regExp, WebElement webElement) {
        return BotUtils.matches(regExp, textIn(webElement));
    }

    public static boolean textNotMatches(String regExp, WebElement webElement) {
        return BotUtils.notMatches(regExp, textIn(webElement));
    }

    public static void assertHasText(WebElement webElement) {
        if (hasNotText(webElement)) {
            throw new WebAssertionError("Element has no text", webElement);
        }
    }

    public static void assertHasNotText(WebElement webElement) {
        if (hasText(webElement)) {
            throw new WebAssertionError("Element has text " + com.github.webdriverextensions.internal.utils.StringUtils.quote(textIn(webElement)) + " when it shouldn't", webElement);
        }
    }

    public static void assertTextEquals(String text, WebElement webElement) {
        BotUtils.assertEquals("Text", text, textIn(webElement), webElement);
    }

    public static void assertTextNotEquals(String text, WebElement webElement) {
        BotUtils.assertNotEquals("Text", text, textIn(webElement), webElement);
    }

    public static void assertTextEqualsIgnoreCase(String text, WebElement webElement) {
        BotUtils.assertEqualsIgnoreCase("Text", text, textIn(webElement), webElement);
    }

    public static void assertTextNotEqualsIgnoreCase(String text, WebElement webElement) {
        BotUtils.assertNotEqualsIgnoreCase("Text", text, textIn(webElement), webElement);
    }

    public static void assertTextContains(String searchText, WebElement webElement) {
        BotUtils.assertContains("Text", searchText, textIn(webElement), webElement);
    }

    public static void assertTextNotContains(String searchText, WebElement webElement) {
        BotUtils.assertNotContains("Text", searchText, textIn(webElement), webElement);
    }

    public static void assertTextContainsIgnoreCase(String searchText, WebElement webElement) {
        BotUtils.assertContainsIgnoreCase("Text", searchText, textIn(webElement), webElement);
    }

    public static void assertTextNotContainsIgnoreCase(String searchText, WebElement webElement) {
        BotUtils.assertNotContainsIgnoreCase("Text", searchText, textIn(webElement), webElement);
    }

    public static void assertTextStartsWith(String prefix, WebElement webElement) {
        BotUtils.assertStartsWith("Text", prefix, textIn(webElement), webElement);
    }

    public static void assertTextNotStartsWith(String prefix, WebElement webElement) {
        BotUtils.assertNotStartsWith("Text", prefix, textIn(webElement), webElement);
    }

    public static void assertTextStartsWithIgnoreCase(String prefix, WebElement webElement) {
        BotUtils.assertStartsWithIgnoreCase("Text", prefix, textIn(webElement), webElement);
    }

    public static void assertTextNotStartsWithIgnoreCase(String prefix, WebElement webElement) {
        BotUtils.assertNotStartsWithIgnoreCase("Text", prefix, textIn(webElement), webElement);
    }

    public static void assertTextEndsWith(String suffix, WebElement webElement) {
        BotUtils.assertEndsWith("Text", suffix, textIn(webElement), webElement);
    }

    public static void assertTextNotEndsWith(String suffix, WebElement webElement) {
        BotUtils.assertNotEndsWith("Text", suffix, textIn(webElement), webElement);
    }

    public static void assertTextEndsWithIgnoreCase(String suffix, WebElement webElement) {
        BotUtils.assertEndsWithIgnoreCase("Text", suffix, textIn(webElement), webElement);
    }

    public static void assertTextNotEndsWithIgnoreCase(String suffix, WebElement webElement) {
        BotUtils.assertNotEndsWithIgnoreCase("Text", suffix, textIn(webElement), webElement);
    }

    public static void assertTextMatches(String regExp, WebElement webElement) {
        BotUtils.assertMatches("Text", regExp, textIn(webElement), webElement);
    }

    public static void assertTextNotMatches(String regExp, WebElement webElement) {
        BotUtils.assertNotMatches("Text", regExp, textIn(webElement), webElement);
    }

    public static double textInAsNumber(WebElement webElement) {
        return org.apache.commons.lang3.math.NumberUtils.createDouble(textIn(webElement));
    }

    public static boolean textIsNumber(WebElement webElement) {
        try {
            textInAsNumber(webElement);
            return true;
        } catch (NumberFormatException var2) {
            return false;
        }
    }

    public static boolean textIsNotNumber(WebElement webElement) {
        return !textIsNumber(webElement);
    }

    public static boolean textEquals(double number, WebElement webElement) {
        return BotUtils.isEqual(number, textInAsNumber(webElement));
    }

    public static boolean textNotEquals(double number, WebElement webElement) {
        return BotUtils.notEquals(number, textInAsNumber(webElement));
    }

    public static boolean textLessThan(double number, WebElement webElement) {
        return BotUtils.lessThan(number, textInAsNumber(webElement));
    }

    public static boolean textLessThanOrEquals(double number, WebElement webElement) {
        return BotUtils.lessThanOrEquals(number, textInAsNumber(webElement));
    }

    public static boolean textGreaterThan(double number, WebElement webElement) {
        return BotUtils.greaterThan(number, textInAsNumber(webElement));
    }

    public static boolean textGreaterThanOrEquals(double number, WebElement webElement) {
        return BotUtils.greaterThanOrEquals(number, textInAsNumber(webElement));
    }

    public static void assertTextIsNumber(WebElement webElement) {
        if (textIsNotNumber(webElement)) {
            throw new WebAssertionError("Element text is not a number", webElement);
        }
    }

    public static void assertTextIsNotNumber(WebElement webElement) {
        if (textIsNumber(webElement)) {
            throw new WebAssertionError("Element text is a number when it shouldn't", webElement);
        }
    }

    public static void assertTextEquals(double number, WebElement webElement) {
        BotUtils.assertEquals("Text", number, textInAsNumber(webElement), webElement);
    }

    public static void assertTextNotEquals(double number, WebElement webElement) {
        BotUtils.assertNotEquals("Text", number, textInAsNumber(webElement), webElement);
    }

    public static void assertTextLessThan(double number, WebElement webElement) {
        BotUtils.assertLessThan("Text", number, textInAsNumber(webElement), webElement);
    }

    public static void assertTextLessThanOrEquals(double number, WebElement webElement) {
        BotUtils.assertLessThanOrEquals("Text", number, textInAsNumber(webElement), webElement);
    }

    public static void assertTextGreaterThan(double number, WebElement webElement) {
        BotUtils.assertGreaterThan("Text", number, textInAsNumber(webElement), webElement);
    }

    public static void assertTextGreaterThanOrEquals(double number, WebElement webElement) {
        BotUtils.assertGreaterThanOrEquals("Text", number, textInAsNumber(webElement), webElement);
    }

    public static boolean isSelected(WebElement webElement) {
        return webElement.isSelected();
    }

    public static boolean isDeselected(WebElement webElement) {
        return !isSelected(webElement);
    }

    public static void assertIsSelected(WebElement webElement) {
        if (isDeselected(webElement)) {
            throw new WebAssertionError("Element is not selected", webElement);
        }
    }

    public static void assertIsDeselected(WebElement webElement) {
        if (isSelected(webElement)) {
            throw new WebAssertionError("Element is not deselected", webElement);
        }
    }

    public static boolean isChecked(WebElement webElement) {
        return webElement.isSelected();
    }

    public static boolean isUnchecked(WebElement webElement) {
        return !isChecked(webElement);
    }

    public static void assertIsChecked(WebElement webElement) {
        if (isUnchecked(webElement)) {
            throw new WebAssertionError("Element is not checked", webElement);
        }
    }

    public static void assertIsUnchecked(WebElement webElement) {
        if (isChecked(webElement)) {
            throw new WebAssertionError("Element is not unchecked", webElement);
        }
    }

    public static boolean isEnabled(WebElement webElement) {
        return webElement.isEnabled();
    }

    public static boolean isDisabled(WebElement webElement) {
        return !isEnabled(webElement);
    }

    public static void assertIsEnabled(WebElement webElement) {
        if (isDisabled(webElement)) {
            throw new WebAssertionError("Element is not enabled", webElement);
        }
    }

    public static void assertIsDisabled(WebElement webElement) {
        if (isEnabled(webElement)) {
            throw new WebAssertionError("Element is not disabled", webElement);
        }
    }

    public static boolean hasOption(String text, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!textEquals(text, option));

        return true;
    }

    public static boolean hasNotOption(String text, WebElement webElement) {
        return !hasOption(text, webElement);
    }

    public static boolean optionIsEnabled(String text, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!textEquals(text, option) || !isEnabled(option));

        return true;
    }

    public static boolean optionIsDisabled(String text, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!textEquals(text, option) || !isDisabled(option));

        return true;
    }

    public static String getSelectedOption(WebElement webElement) {
        Select s = new Select(webElement);
        return s.getFirstSelectedOption().getText();
    }

    public static boolean optionIsSelected(String text, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!textEquals(text, option) || !isSelected(option));

        return true;
    }

    public static boolean optionIsDeselected(String text, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!textEquals(text, option) || !isDeselected(option));

        return true;
    }

    public static boolean allOptionsAreSelected(WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var2 = options.iterator();

        WebElement option;
        do {
            if (!var2.hasNext()) {
                return true;
            }

            option = (WebElement) var2.next();
        } while (!isDeselected(option));

        return false;
    }

    public static boolean noOptionIsSelected(WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var2 = options.iterator();

        WebElement option;
        do {
            if (!var2.hasNext()) {
                return true;
            }

            option = (WebElement) var2.next();
        } while (!isSelected(option));

        return false;
    }

    public static void assertHasOption(String text, WebElement webElement) {
        if (hasNotOption(text, webElement)) {
            throw new WebAssertionError("Element has no option " + com.github.webdriverextensions.internal.utils.StringUtils.quote(text.trim()), webElement);
        }
    }

    public static void assertHasNotOption(String text, WebElement webElement) {
        if (hasOption(text, webElement)) {
            throw new WebAssertionError("Element has option " + com.github.webdriverextensions.internal.utils.StringUtils.quote(text.trim()) + " when it shouldn't", webElement);
        }
    }

    public static void assertOptionIsEnabled(String text, WebElement webElement) {
        assertHasOption(text, webElement);
        if (optionIsDisabled(text, webElement)) {
            throw new WebAssertionError("Option " + com.github.webdriverextensions.internal.utils.StringUtils.quote(text.trim()) + " is not enabled", webElement);
        }
    }

    public static void assertOptionIsDisabled(String text, WebElement webElement) {
        assertHasOption(text, webElement);
        if (optionIsEnabled(text, webElement)) {
            throw new WebAssertionError("Option " + com.github.webdriverextensions.internal.utils.StringUtils.quote(text.trim()) + " is not disabled", webElement);
        }
    }

    public static void assertOptionIsSelected(String text, WebElement webElement) {
        assertHasOption(text, webElement);
        if (optionIsDeselected(text, webElement)) {
            throw new WebAssertionError("Option " + com.github.webdriverextensions.internal.utils.StringUtils.quote(text.trim()) + " is not selected", webElement);
        }
    }

    public static void assertOptionIsDeselected(String text, WebElement webElement) {
        assertHasOption(text, webElement);
        if (optionIsSelected(text, webElement)) {
            throw new WebAssertionError("Option " + com.github.webdriverextensions.internal.utils.StringUtils.quote(text.trim()) + " is not deselected", webElement);
        }
    }

    public static void assertAllOptionsAreSelected(WebElement webElement) {
        if (!allOptionsAreSelected(webElement)) {
            throw new WebAssertionError("All options are not selected", webElement);
        }
    }

    public static void assertNoOptionIsSelected(WebElement webElement) {
        if (!noOptionIsSelected(webElement)) {
            throw new WebAssertionError("All options are not deselected", webElement);
        }
    }

    public static boolean hasOptionWithValue(String value, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!valueEquals(value, option));

        return true;
    }

    public static boolean hasNotOptionWithValue(String value, WebElement webElement) {
        return !hasOptionWithValue(value, webElement);
    }

    public static boolean optionWithValueIsEnabled(String value, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!valueEquals(value, option) || !isEnabled(option));

        return true;
    }

    public static boolean optionWithValueIsDisabled(String value, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!valueEquals(value, option) || !isDisabled(option));

        return true;
    }

    public static boolean optionWithValueIsSelected(String value, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!valueEquals(value, option) || !isSelected(option));

        return true;
    }

    public static boolean optionWithValueIsDeselected(String value, WebElement webElement) {
        List<WebElement> options = (new Select(webElement)).getOptions();
        Iterator var3 = options.iterator();

        WebElement option;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            option = (WebElement) var3.next();
        } while (!valueEquals(value, option) || !isDeselected(option));

        return true;
    }

    public static void assertHasOptionWithValue(String value, WebElement webElement) {
        if (hasNotOptionWithValue(value, webElement)) {
            throw new WebAssertionError("Element has no option with value " + com.github.webdriverextensions.internal.utils.StringUtils.quote(value.trim()), webElement);
        }
    }

    public static void assertHasNotOptionWithValue(String value, WebElement webElement) {
        if (hasOptionWithValue(value, webElement)) {
            throw new WebAssertionError("Element has option with value " + com.github.webdriverextensions.internal.utils.StringUtils.quote(value.trim()) + " when it shouldn't", webElement);
        }
    }

    public static void assertOptionWithValueIsEnabled(String value, WebElement webElement) {
        assertHasOptionWithValue(value, webElement);
        if (optionWithValueIsDisabled(value, webElement)) {
            throw new WebAssertionError("Option with value " + com.github.webdriverextensions.internal.utils.StringUtils.quote(value.trim()) + " is not enabled", webElement);
        }
    }

    public static void assertOptionWithValueIsDisabled(String value, WebElement webElement) {
        assertHasOptionWithValue(value, webElement);
        if (optionWithValueIsEnabled(value, webElement)) {
            throw new WebAssertionError("Option with value " + com.github.webdriverextensions.internal.utils.StringUtils.quote(value.trim()) + " is not disabled", webElement);
        }
    }

    public static void assertOptionWithValueIsSelected(String value, WebElement webElement) {
        assertHasOptionWithValue(value, webElement);
        if (optionWithValueIsDeselected(value, webElement)) {
            throw new WebAssertionError("Option with value " + com.github.webdriverextensions.internal.utils.StringUtils.quote(value.trim()) + " is not selected", webElement);
        }
    }

    public static void assertOptionWithValueIsDeselected(String value, WebElement webElement) {
        assertHasOptionWithValue(value, webElement);
        if (optionWithValueIsSelected(value, webElement)) {
            throw new WebAssertionError("Option with value " + com.github.webdriverextensions.internal.utils.StringUtils.quote(value.trim()) + " is not deselected", webElement);
        }
    }

    public static boolean hasOptionWithIndex(int index, WebElement webElement) {
        List options = (new Select(webElement)).getOptions();

        try {
            return options.get(index) != null;
        } catch (IndexOutOfBoundsException var4) {
            return false;
        }
    }

    public static boolean hasNotOptionWithIndex(int index, WebElement webElement) {
        return !hasOptionWithIndex(index, webElement);
    }

    public static boolean optionWithIndexIsEnabled(int index, WebElement webElement) {
        List options = (new Select(webElement)).getOptions();

        try {
            return isEnabled((WebElement) options.get(index));
        } catch (IndexOutOfBoundsException var4) {
            return false;
        }
    }

    public static boolean optionWithIndexIsDisabled(int index, WebElement webElement) {
        List options = (new Select(webElement)).getOptions();

        try {
            return isDisabled((WebElement) options.get(index));
        } catch (IndexOutOfBoundsException var4) {
            return false;
        }
    }

    public static boolean optionWithIndexIsSelected(int index, WebElement webElement) {
        List options = (new Select(webElement)).getOptions();

        try {
            return isSelected((WebElement) options.get(index));
        } catch (IndexOutOfBoundsException var4) {
            return false;
        }
    }

    public static boolean optionWithIndexIsDeselected(int index, WebElement webElement) {
        List options = (new Select(webElement)).getOptions();

        try {
            return isDeselected((WebElement) options.get(index));
        } catch (IndexOutOfBoundsException var4) {
            return false;
        }
    }

    public static void assertHasOptionWithIndex(int index, WebElement webElement) {
        if (hasNotOptionWithIndex(index, webElement)) {
            throw new WebAssertionError("Element  has no option with index " + com.github.webdriverextensions.internal.utils.StringUtils.quote((double) index), webElement);
        }
    }

    public static void assertHasNotOptionWithIndex(int index, WebElement webElement) {
        if (hasOptionWithIndex(index, webElement)) {
            throw new WebAssertionError("Element has option with index " + com.github.webdriverextensions.internal.utils.StringUtils.quote((double) index) + " when it shouldn't", webElement);
        }
    }

    public static void assertOptionWithIndexIsEnabled(int index, WebElement webElement) {
        assertHasOptionWithIndex(index, webElement);
        if (optionWithIndexIsDisabled(index, webElement)) {
            throw new WebAssertionError("Option with index " + com.github.webdriverextensions.internal.utils.StringUtils.quote((double) index) + " is not enabled", webElement);
        }
    }

    public static void assertOptionWithIndexIsDisabled(int index, WebElement webElement) {
        assertHasOptionWithIndex(index, webElement);
        if (optionWithIndexIsEnabled(index, webElement)) {
            throw new WebAssertionError("Option with index " + com.github.webdriverextensions.internal.utils.StringUtils.quote((double) index) + " is not disabled", webElement);
        }
    }

    public static void assertOptionWithIndexIsSelected(int index, WebElement webElement) {
        assertHasOptionWithIndex(index, webElement);
        if (optionWithIndexIsDeselected(index, webElement)) {
            throw new WebAssertionError("Option with index " + com.github.webdriverextensions.internal.utils.StringUtils.quote((double) index) + " is not selected", webElement);
        }
    }

    public static void assertOptionWithIndexIsDeselected(int index, WebElement webElement) {
        assertHasOptionWithIndex(index, webElement);
        if (optionWithIndexIsSelected(index, webElement)) {
            throw new WebAssertionError("Option with index " + com.github.webdriverextensions.internal.utils.StringUtils.quote((double) index) + " is not deselected", webElement);
        }
    }

    public static <T> boolean is(T actual, Matcher<? super T> matcher) {
        try {
            assertThat(actual, matcher);
            return true;
        } catch (AssertionError var3) {
            return false;
        }
    }

    public static <T> void assertThat(T actual, Matcher<? super T> matcher) {
        MatcherAssert.assertThat(actual, matcher);
    }

    public static <T> void assertThat(String reason, T actual, Matcher<? super T> matcher) {
        MatcherAssert.assertThat(reason, actual, matcher);
    }
}
