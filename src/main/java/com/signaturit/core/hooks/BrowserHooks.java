package com.signaturit.core.hooks;

import com.signaturit.core.selenium.BrowseFactory;
import com.signaturit.core.selenium.BrowserInstance;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * TODO Browser init and finalize
 * To instanciate the browser according and finalize it.
 */
public class BrowserHooks {

    private static final Logger log = LoggerFactory.getLogger(com.github.webdriverextensions.Bot.class);

    private final static String resourcePath = "resources/selenium.properties";

    private Properties loadExecutionProperties() {
        Properties prop = new Properties();

        try (InputStream input = new FileInputStream(resourcePath)) {
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return prop;
    }

    @Before(order = 0)
    public void startBrowser() {
        Properties props = loadExecutionProperties();

        BrowseFactory b = new BrowseFactory(props);
        b.openBrowser();

        BrowserInstance.getDriver().manage().window().maximize();
        BrowserInstance.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        log.info("Browser opened.");
    }

    @After(order = 0)
    public void stopBrowser() {
        BrowserInstance.getDriver().close();
        BrowserInstance.getDriver().quit();
        log.info("Web driver stoppetd.");
        log.info("-- Test finished --");
    }

}
