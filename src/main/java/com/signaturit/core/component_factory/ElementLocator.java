package com.signaturit.core.component_factory;

import com.github.webdriverextensions.annotations.ResetSearchContext;
import org.openqa.selenium.*;

import java.lang.reflect.Field;
import java.util.List;

public class ElementLocator implements org.openqa.selenium.support.pagefactory.ElementLocator {

    public final SearchContext searchContext;
    private final WebDriver driver;
    private final boolean shouldCache;
    private final By by;
    private WebElement cachedElement;
    private List<WebElement> cachedElementList;

    public ElementLocator(SearchContext searchContext, Field field, WebDriver driver) {
        Annotations annotations = new Annotations(field);
        this.driver = driver;
        if (annotations.isSearchContextReset()) {
            this.searchContext = driver;
        } else {
            this.searchContext = searchContext;
        }

//        if(field.getName().contains("filter") || field.getName().contains("consultationList") || field.getName().contains("overview")) {
//            List<WebElement> e = this.searchContext.findElements(By.cssSelector("*"));
//            for (WebElement a : e) {
//                System.out.println("TAG " + a.getTagName() + " ID: " + a.getAttribute("id"));
//            }
//        }

        shouldCache = annotations.isLookupCached();
        by = annotations.buildBy();
    }

    @Override
    public WebElement findElement() {
        WebElement element;
        try {
            if (cachedElement != null && shouldCache) {
                return cachedElement;
            }
//            if(by.toString().contains("button[id='widget:fGrouper:j_id_1e:j_id_3d:dynaButton")){
//                List<WebElement> e = searchContext.findElements(By.cssSelector("*"));
//                for (WebElement ee : e)
//                    System.out.println("Tag: " + ee.getTagName() + "   -> " + ee.getAttribute("id"));
//            }
            element = searchContext.findElement(by);
            if (shouldCache) {
                cachedElement = element;
            }
        } catch (NoSuchElementException ignore) {
            if (cachedElement != null && shouldCache) {
                return cachedElement;
            }
            //element = searchContext.findElement(by);
//            List<WebElement> e = driver.findElements(By.cssSelector("*"));
//            for(WebElement ee : e)
//                System.out.println("Tag: " + ee.getTagName() + "   -> " + ee.getAttribute("id"));
            element = driver.findElement(by);
            if (shouldCache) {
                cachedElement = element;
            }
        }
        return element;
    }

    @Override
    public List<WebElement> findElements() {
        List<WebElement> elements;
        try {
            if (cachedElementList != null && shouldCache) {
                return cachedElementList;
            }

            elements = searchContext.findElements(by);
            if (shouldCache) {
                cachedElementList = elements;
            }
        } catch (NoSuchElementException ignore) {
            driver.switchTo().defaultContent();
            if (cachedElementList != null && shouldCache) {
                return cachedElementList;
            }

            //elements = searchContext.findElements(by);
            elements = driver.findElements(by);

            if (shouldCache) {
                cachedElementList = elements;
            }
        }

        return elements;
    }

    private boolean hasAnnotatedResetSearchContext(Field field) {
        ResetSearchContext annotation = (ResetSearchContext) field.getAnnotation(ResetSearchContext.class);
        if (annotation != null) {
            return true;
        }
        return false;
    }
}
