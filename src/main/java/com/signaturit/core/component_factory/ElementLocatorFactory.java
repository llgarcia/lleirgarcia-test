package com.signaturit.core.component_factory;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Field;

public class ElementLocatorFactory implements org.openqa.selenium.support.pagefactory.ElementLocatorFactory {

    private WebDriver driver;
    public SearchContext searchContext;

    /**
     * Creates a new element locator.
     *
     * @param searchContext The context to use when finding the element
     *                      value
     */
    public ElementLocatorFactory(SearchContext searchContext, WebDriver driver) {
        this.searchContext = searchContext;
        this.driver = driver;
    }

    @Override
    public org.openqa.selenium.support.pagefactory.ElementLocator createLocator(Field field) {
        return new ElementLocator(searchContext, field, driver);
    }
}
