package com.signaturit.core.component_factory;

import com.github.webdriverextensions.WebDriverExtensionsContext;
import com.signaturit.application.simple_components.Button;
import com.signaturit.application.simple_components.FormInput;
import com.signaturit.application.simple_components.Input;
import com.signaturit.application.simple_components.Label;
import com.signaturit.core.selenium.BrowserInstance;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class PageFactoryCustom {


    private static final Logger log = LoggerFactory.getLogger(PageFactoryCustom.class);
    private static Object pageStatic =null;

    public PageFactoryCustom() {
    }

    public static <T> T initElements(WebDriver driver, Class<T> pageClassToProxy) {
        T page = instantiatePage(driver, pageClassToProxy);
        initElements(driver, page);
        return page;
    }

    private static void initElements(WebDriver driver, Object page) {
        initElements((ElementLocatorFactory)(new DefaultElementLocatorFactory(driver)), (Object)page);
    }

    private static void initElements(ElementLocatorFactory factory, Object page) {
        initElements((FieldDecorator)(new DefaultFieldDecorator(factory)), (Object)page);
    }

    public static void initElements(FieldDecorator decorator, Object page) {
        for(Class proxyIn = page.getClass(); proxyIn != Object.class; proxyIn = proxyIn.getSuperclass()) {
            proxyFields(decorator, page, proxyIn);
        }
        adaptAnnotationsToValueClassFields(page);
    }

    public static void initElementsWithoutAnnotations(FieldDecorator decorator, Object page) {
        for(Class proxyIn = page.getClass(); proxyIn != Object.class; proxyIn = proxyIn.getSuperclass()) {
            proxyFields(decorator, page, proxyIn);
        }
    }


    public static void initElements(FieldDecorator decorator, Object page, boolean isAFrame) {
        for(Class proxyIn = page.getClass(); proxyIn != Object.class; proxyIn = proxyIn.getSuperclass()) {
            proxyFields(decorator, page, proxyIn, isAFrame);
        }
        adaptAnnotationsToValueClassFields(page);
    }

    public void initElements() {
        initElements(new PageFieldDecorator(WebDriverExtensionsContext.getDriver()), this);
    }

    public static void initElements(Object page) {
        initElements(new PageFieldDecorator(BrowserInstance.getDriver()), page);
    }

    private static void proxyFields(FieldDecorator decorator, Object page, Class<?> proxyIn) {
        Field[] fields = proxyIn.getDeclaredFields();
        Field[] var4 = fields;
        int var5 = fields.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            Field field = var4[var6];
            Class<?> a = null;

            Object value = decorator.decorate(page.getClass().getClassLoader(), field);
            if (value != null) {
                try {
                    field.setAccessible(true);
                    field.set(page, value);
                } catch (IllegalAccessException var10) {
                    throw new RuntimeException(var10);
                }
            }
        }
    }

    private static void proxyFields(FieldDecorator decorator, Object page, Class<?> proxyIn, boolean isAFrame) {
        Field[] fieldsPageStaticNewInstance = new Field[0];

        Class<?> a = null;
        try {
            a = Class.forName(page.getClass().getCanonicalName());
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage());
        }

        try {
            if (a != null){
                pageStatic = a.newInstance();
                fieldsPageStaticNewInstance = pageStatic.getClass().getDeclaredFields();
            }
        } catch (NullPointerException | InstantiationException | IllegalAccessException e) {
            log.error(e.getMessage());
        }

        for(Field field : fieldsPageStaticNewInstance){
            Object value = decorator.decorate(page.getClass().getClassLoader(), field);
            if (value != null) {
                try {
                    field.setAccessible(true);
                    field.set(pageStatic, value);
                } catch (IllegalAccessException var10) {
                    throw new RuntimeException(var10);
                }
            }
        }
    }

    private static <T> T instantiatePage(WebDriver driver, Class<T> pageClassToProxy) {
        try {
            try {
                Constructor<T> constructor = pageClassToProxy.getConstructor(WebDriver.class);
                return constructor.newInstance(driver);
            } catch (NoSuchMethodException var3) {
                return pageClassToProxy.newInstance();
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException var4) {
            throw new RuntimeException(var4);
        }
    }

    /**
     * This method run every class was instanciate by the method PageFactory.initElements, get all the fields inner the class,
     * read ComponentInfo annotations and get the attributes and set in their superclasses.
     *
     * All of those fields will has a superclass with those atributes.
     *
     * @param objType Represents a class with fields.
     */
    private static void adaptAnnotationsToValueClassFields(Object objType) {
        Class<?> clazzToGettFields = objType.getClass();
        Field[] fields = clazzToGettFields.getFields();

        Object fieldObjectDeclared;
        ComponentInfo annotationWanted;
        FindBy annotationWantedFindBy;

        if(!StringUtils.containsIgnoreCase(clazzToGettFields.getSimpleName(), "logger") &&
                !StringUtils.containsIgnoreCase(clazzToGettFields.getSimpleName(), "string") &&
                !StringUtils.containsIgnoreCase(clazzToGettFields.getSimpleName(), "integer") &&
                !StringUtils.containsIgnoreCase(clazzToGettFields.getSimpleName(), "boolean")
        ){

            for(Field fieldDeclared : fields){

                try {
                    fieldDeclared.setAccessible(true);
                    fieldObjectDeclared = fieldDeclared.get(objType);

                    Object a = fieldDeclared.getType();

                    String fieldName = fieldDeclared.getName();

                    boolean isElementInList = StringUtils.containsIgnoreCase(fieldName, "elementinlist");

                    if(StringUtils.equals(fieldName, "list"))
                        return;

                    annotationWanted = fieldDeclared.getAnnotation(ComponentInfo.class);
                    annotationWantedFindBy = fieldDeclared.getAnnotation(FindBy.class);

                    if(annotationWanted != null) {

                        if (fieldObjectDeclared instanceof Input) {
                            ((Input) fieldObjectDeclared).setMandatoy(annotationWanted.mandatory());
                        }

                        if (fieldObjectDeclared instanceof Button
                                || fieldObjectDeclared instanceof Label
                                || fieldObjectDeclared instanceof FormInput) {

                            ((BaseWebComponent) fieldObjectDeclared).setName(annotationWanted.nameField());
                            ((BaseWebComponent) fieldObjectDeclared).setContext(annotationWanted.appContext());
                            ((BaseWebComponent) fieldObjectDeclared).setComponentType(annotationWanted.componentType());
                            ((BaseWebComponent) fieldObjectDeclared).setLocator(annotationWantedFindBy.css() + annotationWantedFindBy.xpath());
                        }
                    }
                    else{
//                        log.info("No [ComponentInfo] annotation is in field -> ["+objType.getClass().getCanonicalName() + " -> " +fieldObjectDeclared.getClass().getCanonicalName()+"].");
                    }
                } catch (IllegalAccessException | ClassCastException e) {
                    log.error(e.getMessage());
                }
            }
        }
    }
}
