package com.signaturit.core.component_factory;

import com.signaturit.core.selenium.BrowserInstance;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class SwitchManager {

    private Stack<WebElement> switchStack = new Stack<WebElement>();

    public void enter(WebElement e) {
        switchStack.push(e);
        enterStack();
    }

    private void enterStack() {
        Set<WebElement> memory = new HashSet<WebElement>();

        for (WebElement webElement : switchStack) {
            if (!memory.contains(webElement)) {
                memory.add(webElement);
                try {
                    BrowserInstance.getDriver().switchTo().frame(webElement);

                    List<WebElement> webs = BrowserInstance.getDriver().findElements(By.cssSelector("iframe"));

                    for(WebElement e : webs){
                        if(StringUtils.contains(e.getTagName(), "iframe") && StringUtils.contains(e.getAttribute("id"), "idIfr")) {
                            break;
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
    }
}