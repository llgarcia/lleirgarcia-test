package com.signaturit.core.component_factory;

import com.github.webdriverextensions.WebComponent;

public class BaseWebComponent extends WebComponent {

    protected String name;
    protected String context;
    protected String componentType;
    protected String locator;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getComponentType() {
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public String getLocator() {
        return locator;
    }

    public void setLocator(String locator) {
        this.locator = locator;
    }
}
