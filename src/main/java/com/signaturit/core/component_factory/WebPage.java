package com.signaturit.core.component_factory;

import com.github.webdriverextensions.WebDriverExtensionsContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.FieldDecorator;


public class WebPage {

    public void initElements() {
        PageFactoryCustom.initElements(new PageFieldDecorator(WebDriverExtensionsContext.getDriver()), this);
    }

    void initElements(WebDriver driver) {
        PageFactoryCustom.initElements(new PageFieldDecorator(driver), this);
    }

    void initElements(FieldDecorator decorator) {
        PageFactoryCustom.initElements(decorator, this);
    }


}
