package com.signaturit.core.component_factory;

import com.signaturit.core.selenium.BrowserInstance;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.FieldDecorator;


public class WebSite {

    public void initElements() {
        PageFactoryCustom.initElements(new PageFieldDecorator(BrowserInstance.getDriver()), this);
    }

    public void initElements(Object clas) {
        PageFactoryCustom.initElements(new PageFieldDecorator(BrowserInstance.getDriver()), clas);
    }

    public void initElements(WebDriver driver) {
        PageFactoryCustom.initElements(new PageFieldDecorator(driver), this);
    }

    public void initElements(FieldDecorator decorator) {
        PageFactoryCustom.initElements(decorator, this);
    }

}
