package com.signaturit.core.component_factory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ComponentInfo {

    String nameField() default "";

    String appContext() default "";

    String componentType() default "";

    String locator() default "";

    String defaultValue() default "";

    boolean mandatory() default false;

}
