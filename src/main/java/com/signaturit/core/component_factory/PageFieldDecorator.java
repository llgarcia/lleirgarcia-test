package com.signaturit.core.component_factory;

import com.github.webdriverextensions.WebComponent;
import com.github.webdriverextensions.WebRepository;
import com.github.webdriverextensions.internal.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;

import static com.github.webdriverextensions.internal.ReflectionUtils.getListType;

public class PageFieldDecorator extends DefaultFieldDecorator {


    private boolean isSwitchTo = false;
    private SearchContext searchContext;
    private final WebDriver driver;
    private final ObjectPool pool;
    private final WebComponentFactory webComponentFactory;
    private final WebComponentListFactory webComponentListFactory;
    private ParameterizedType genericTypeArguments;

    void setGenericTypeArguments(ParameterizedType genericTypeArguments) {
        this.genericTypeArguments = genericTypeArguments;
    }

    public PageFieldDecorator(final WebDriver driver) {
        super(new ElementLocatorFactory(driver, driver));
        this.searchContext = driver;
        this.driver = driver;
        this.pool = new ObjectPool(driver);
        this.webComponentFactory = new DefaultWebComponentFactory();
        this.webComponentListFactory = new DefaultWebComponentListFactory(webComponentFactory);
        this.genericTypeArguments = null;
    }

    public PageFieldDecorator(final SearchContext searchContext, final WebDriver driver) {
        super(new ElementLocatorFactory(searchContext, driver));
        this.driver = driver;
        this.pool = new ObjectPool(driver);
        this.webComponentFactory = new DefaultWebComponentFactory();
        this.webComponentListFactory = new DefaultWebComponentListFactory(webComponentFactory);
        this.genericTypeArguments = null;
    }

    private PageFieldDecorator(final SearchContext searchContext, final WebDriver driver, final ParameterizedType genericTypeArguments) {
        super(new ElementLocatorFactory(searchContext, driver));
        this.driver = driver;
        this.pool = new ObjectPool(driver);
        this.webComponentFactory = new DefaultWebComponentFactory();
        this.webComponentListFactory = new DefaultWebComponentListFactory(webComponentFactory);
        this.genericTypeArguments = genericTypeArguments;
    }

    @Override
    public Object decorate(ClassLoader loader, Field field) {
        try {
            if (isDecorableWebComponent(field)) {
                if (field.getGenericType() instanceof TypeVariable) {
                    return decorateWebComponent(loader, field, genericTypeArguments);
                } else if (field.getGenericType() instanceof ParameterizedType) {
                    return decorateWebComponent(loader, field, (ParameterizedType) field.getGenericType());
                } else {
                    return decorateWebComponent(loader, field, null);
                }
            }
            if (isDecorableWebComponentList(field)) {
                Type listType = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                if (listType instanceof TypeVariable) {
                    return decorateWebComponentList(loader, field, genericTypeArguments);
                } else if (listType instanceof ParameterizedType) {
                    return decorateWebComponentList(loader, field, (ParameterizedType) listType);
                } else {
                    return decorateWebComponentList(loader, field, null);
                }
            }

            if (isDecorableSiteObject(field)) {
                return pool.getSiteObject(field, this);
            }
            if (isDecorablePageObject(field)) {
                return pool.getPageObject(field, this);
            }
            if (isDecorableRepositoryObject(field)) {
                return pool.getRepositoryObject(field, this);
            }
            if ("wrappedWebElement".equals(field.getName())) {
                return null;
            }
            if ("delegateWebElement".equals(field.getName())) {
                return null;
            }
            return super.decorate(loader, field);
        } catch (Exception ex) {

            if (ex instanceof WebDriverExtensionException) {
                throw (WebDriverExtensionException) ex; // re-throw it
            } else {
                throw new WebDriverExtensionException("Failed to decorate field " + field.getName() + " in class " + field.getDeclaringClass(), ex);
            }
        }
    }

    private boolean isDecorableWebComponent(Field field) {
        return WebComponent.class.isAssignableFrom(field.getType());
    }

    private boolean isDecorableWebComponentList(Field field) {
        if (!List.class.isAssignableFrom(field.getType())) {
            return false;
        }

        // Type erasure in Java isn't complete. Attempt to discover the generic
        // type of the list.
        Type genericType = field.getGenericType();
        if (!(genericType instanceof ParameterizedType)) {
            return false;
        }

        Type listType = ((ParameterizedType) genericType).getActualTypeArguments()[0];

        if (listType instanceof TypeVariable) {
            if (!WebComponent.class.isAssignableFrom(getListType(field, genericTypeArguments))) {
                return false;
            }
        } else if (listType instanceof ParameterizedType) {
            if (!WebComponent.class.isAssignableFrom((Class) ((ParameterizedType) listType).getRawType())) {
                return false;
            }
        } else {
            if (!WebComponent.class.isAssignableFrom((Class) listType)) {
                return false;
            }
        }

        return field.getAnnotation(FindBy.class) != null
                || field.getAnnotation(FindBys.class) != null
                || field.getAnnotation(FindAll.class) != null;
    }

    private boolean isDecorableFrameObject(Field field) {
        return WebFrame.class.isAssignableFrom(field.getType());
    }

    private boolean isDecorablePageObject(Field field) {
        return WebPage.class.isAssignableFrom(field.getType());
    }

    private boolean isDecorableSiteObject(Field field) {
        return WebSite.class.isAssignableFrom(field.getType());
    }

    private boolean isDecorableRepositoryObject(Field field) {
        return WebRepository.class.isAssignableFrom(field.getType());
    }

    private void enterInFrame(ClassLoader loader, Field field) {
        ElementLocator locator = factory.createLocator(field);
        final WebElement webElement = proxyForLocator(loader, locator);
        SwitchManager switchManager = new SwitchManager();
        try {
            if (field.isAnnotationPresent(SwitchToFrame.class) && webElement.isDisplayed()) {
                switchManager.enter(webElement);
            }
        } catch (NoSuchElementException ignore) {
        }
        // aqui se puede especificar el locator como el frame
    }

    private Object decorateWebComponent(ClassLoader loader, Field field, ParameterizedType genericTypeArguments) {
        // aqui en el factory deberia de tener el search context == frame, si tiene
        ElementLocator locator = factory.createLocator(field);
        Class type = ReflectionUtils.getType(field, genericTypeArguments);
        final WebElement webElement = proxyForLocator(loader, locator);
        final WebComponent webComponent = webComponentFactory.create(type, webElement);
        PageFactoryCustom.initElements(new PageFieldDecorator(webElement, driver, genericTypeArguments), webComponent);
        webComponent.init(webComponent.getWrappedWebElement(), WebDriverExtensionAnnotations.getDelagate(webComponent));
        return webComponent;
    }

    private Object decorateWebComponentList(final ClassLoader loader, final Field field, ParameterizedType genericTypeArguments) {
        ElementLocator locator = factory.createLocator(field);
        Class listType = ReflectionUtils.getListType(field, genericTypeArguments);
        List<WebElement> webElements = proxyForListLocator(loader, locator);
        return webComponentListFactory.create(listType, webElements, driver, genericTypeArguments);
    }
}
