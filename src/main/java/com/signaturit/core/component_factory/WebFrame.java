package com.signaturit.core.component_factory;

import org.openqa.selenium.support.pagefactory.FieldDecorator;

public class WebFrame {

    public void initElements(FieldDecorator decorator) {
        PageFactoryCustom.initElements(decorator, this, true);
    }

}
